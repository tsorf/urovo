package ru.frosteye.urovotest.app.di.component;

import ru.frosteye.urovotest.app.di.module.AppModule;
import ru.frosteye.urovotest.app.di.module.PresenterModule;
import ru.frosteye.urovotest.app.di.module.RepoModule;

import javax.inject.Singleton;

import dagger.Component;
import ru.frosteye.urovotest.app.environment.CodeParser;

@Component(modules = {AppModule.class, RepoModule.class})
@Singleton
public interface AppComponent {
    PresenterComponent plus(PresenterModule module);

    CodeParser codeParser();
}