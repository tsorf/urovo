package ru.frosteye.urovotest.app.di.component;

import ru.frosteye.urovotest.app.di.module.PresenterModule;
import ru.frosteye.urovotest.app.di.scope.PresenterScope;
import ru.frosteye.urovotest.presentation.view.impl.activity.BaseActivity;
import ru.frosteye.urovotest.presentation.view.impl.activity.CameraScanActivity;
import ru.frosteye.urovotest.presentation.view.impl.activity.LoginActivity;
import ru.frosteye.urovotest.presentation.view.impl.activity.MainActivity;
import ru.frosteye.urovotest.presentation.view.impl.activity.ModuleActivity;
import ru.frosteye.urovotest.presentation.view.impl.fragment.BaseFragment;

import dagger.Subcomponent;
import ru.frosteye.urovotest.presentation.view.impl.fragment.MenuFragment;
import ru.frosteye.urovotest.presentation.view.impl.fragment.RfidFragment;
import ru.frosteye.urovotest.presentation.view.impl.fragment.ScanFragment;
import ru.frosteye.urovotest.presentation.view.impl.fragment.SettingsFragment;
import ru.frosteye.urovotest.presentation.view.impl.fragment.TagsFragment;

@PresenterScope
@Subcomponent(modules = PresenterModule.class)
public interface PresenterComponent {

    void inject(BaseActivity baseActivity);
    void inject(LoginActivity activity);
    void inject(CameraScanActivity activity);
    void inject(MainActivity activity);
    void inject(ModuleActivity activity);


    void inject(BaseFragment baseFragment);
    void inject(ScanFragment fragment);
    void inject(RfidFragment fragment);
    void inject(SettingsFragment fragment);
    void inject(TagsFragment fragment);
    void inject(MenuFragment fragment);
}
