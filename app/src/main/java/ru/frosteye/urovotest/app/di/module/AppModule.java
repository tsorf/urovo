package ru.frosteye.urovotest.app.di.module;

import ru.frosteye.ovsa.di.qualifer.ApiUrl;
import ru.frosteye.ovsa.execution.network.client.IdentityProvider;
import ru.frosteye.urovotest.R;
import ru.frosteye.urovotest.app.environment.Urovo;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.frosteye.ovsa.di.module.BaseAppModule;
import ru.frosteye.urovotest.data.repo.contract.SettingsRepo;
import ru.frosteye.urovotest.data.repo.contract.TokenRepo;
import ru.frosteye.urovotest.data.repo.contract.UserRepo;
import ru.frosteye.urovotest.execution.exchange.common.Api;
import ru.frosteye.urovotest.execution.exchange.common.ApiClient;
import ru.frosteye.urovotest.execution.exchange.common.RetrofitApiClient;

import static ru.frosteye.ovsa.data.storage.ResourceHelper.*;


@Module
public class AppModule extends BaseAppModule<Urovo> {

    public AppModule(Urovo context) {
        super(context);
    }

    @Provides @ApiUrl
    String provideApiUrl(SettingsRepo repo) {
        return repo.load().getServerUrl();
    }

    @Provides
    Api provideApi(ApiClient apiClient) {
        return apiClient.getApi();
    }

    @Provides @Singleton
    ApiClient provideApiClient(RetrofitApiClient apiClient) {
        return apiClient;
    }

    @Provides @Singleton
    IdentityProvider provideIdentityProvider(TokenRepo userRepo) {
        return userRepo;
    }
}
