package ru.frosteye.urovotest.app.di.module;

import android.view.View;

import ru.frosteye.urovotest.app.di.scope.PresenterScope;
import ru.frosteye.urovotest.presentation.presenter.contract.CameraScanPresenter;
import ru.frosteye.urovotest.presentation.presenter.contract.LoginPresenter;
import ru.frosteye.urovotest.presentation.presenter.contract.MainPresenter;
import ru.frosteye.urovotest.presentation.presenter.contract.MenuPresenter;
import ru.frosteye.urovotest.presentation.presenter.contract.ModulePresenter;
import ru.frosteye.urovotest.presentation.presenter.contract.RfidPresenter;
import ru.frosteye.urovotest.presentation.presenter.contract.ScanPresenter;
import ru.frosteye.urovotest.presentation.presenter.contract.SettingsPresenter;
import ru.frosteye.urovotest.presentation.presenter.contract.TagsPresenter;
import ru.frosteye.urovotest.presentation.presenter.impl.CameraScanPresenterImpl;
import ru.frosteye.urovotest.presentation.presenter.impl.LoginPresenterImpl;
import ru.frosteye.urovotest.presentation.presenter.impl.MainPresenterImpl;
import ru.frosteye.urovotest.presentation.presenter.impl.MenuPresenterImpl;
import ru.frosteye.urovotest.presentation.presenter.impl.ModulePresenterImpl;
import ru.frosteye.urovotest.presentation.presenter.impl.RfidPresenterImpl;
import ru.frosteye.urovotest.presentation.presenter.impl.ScanPresenterImpl;
import ru.frosteye.urovotest.presentation.presenter.impl.SettingsPresenterImpl;
import ru.frosteye.urovotest.presentation.presenter.impl.TagsPresenterImpl;
import ru.frosteye.urovotest.presentation.view.impl.activity.BaseActivity;
import ru.frosteye.urovotest.presentation.view.impl.fragment.BaseFragment;

import dagger.Module;
import dagger.Provides;
import ru.frosteye.ovsa.di.module.BasePresenterModule;

@Module
public class PresenterModule extends BasePresenterModule<BaseActivity, BaseFragment> {
    public PresenterModule(View view) {
        super(view);
    }

    public PresenterModule(BaseActivity activity) {
        super(activity);
    }

    public PresenterModule(BaseFragment fragment) {
        super(fragment);
    }

    @Provides @PresenterScope
    LoginPresenter provideLoginPresenter(LoginPresenterImpl presenter) {
        return presenter;
    }
    
    @Provides @PresenterScope
    ScanPresenter provideScanPresenter(ScanPresenterImpl presenter) {
        return presenter;
    }

    @Provides @PresenterScope
    CameraScanPresenter provideCameraScanPresenter(CameraScanPresenterImpl presenter) {
        return presenter;
    }

    @Provides @PresenterScope
    MainPresenter provideMainPresenter(MainPresenterImpl presenter) {
        return presenter;
    }

    @Provides @PresenterScope
    RfidPresenter provideRfidPresenter(RfidPresenterImpl presenter) {
        return presenter;
    }

    @Provides @PresenterScope
    SettingsPresenter provideSettingsPresenter(SettingsPresenterImpl presenter) {
        return presenter;
    }

    @Provides @PresenterScope
    TagsPresenter provideTagsPresenter(TagsPresenterImpl presenter) {
        return presenter;
    }
    
    @Provides @PresenterScope
    MenuPresenter provideMenuPresenter(MenuPresenterImpl presenter) {
        return presenter;
    }

    @Provides @PresenterScope
    ModulePresenter provideModulePresenter(ModulePresenterImpl presenter) {
        return presenter;
    }
}
