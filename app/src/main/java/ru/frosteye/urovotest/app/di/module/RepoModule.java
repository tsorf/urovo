package ru.frosteye.urovotest.app.di.module;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.frosteye.urovotest.data.repo.contract.CheckRepo;
import ru.frosteye.urovotest.data.repo.contract.NFCRepo;
import ru.frosteye.urovotest.data.repo.contract.SettingsRepo;
import ru.frosteye.urovotest.data.repo.contract.TagsRepo;
import ru.frosteye.urovotest.data.repo.contract.TokenRepo;
import ru.frosteye.urovotest.data.repo.contract.UserRepo;
import ru.frosteye.urovotest.data.repo.impl.AppSettingsRepoImpl;
import ru.frosteye.urovotest.data.repo.impl.CheckRepoImpl;
import ru.frosteye.urovotest.data.repo.impl.NFCRepoImpl;
import ru.frosteye.urovotest.data.repo.impl.TagsRepoImpl;
import ru.frosteye.urovotest.data.repo.impl.TokenRepoImpl;
import ru.frosteye.urovotest.data.repo.impl.UserRepoImpl;

@Module
public class RepoModule {

    @Provides @Singleton
    UserRepo provideUserRepo(UserRepoImpl repo) {
        return repo;
    }

    @Provides @Singleton
    SettingsRepo provideSettingsRepo(AppSettingsRepoImpl repo) {
        return repo;
    }

    @Provides @Singleton
    TagsRepo provideTagsRepo(TagsRepoImpl repo) {
        return repo;
    }

    @Provides @Singleton
    CheckRepo provideCheckRepo(CheckRepoImpl repo) {
        return repo;
    }

    @Provides @Singleton
    NFCRepo provideNFCRepo(NFCRepoImpl repo) {
        return repo;
    }

    @Provides @Singleton
    TokenRepo provideTokenRepo(TokenRepoImpl repo) {
        return repo;
    }
}
