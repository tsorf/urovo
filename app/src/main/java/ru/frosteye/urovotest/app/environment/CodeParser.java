package ru.frosteye.urovotest.app.environment;

import javax.inject.Inject;
import javax.inject.Singleton;

import ru.frosteye.urovotest.R;
import ru.frosteye.urovotest.data.entity.AppSettings;
import ru.frosteye.urovotest.data.entity.Code;
import ru.frosteye.urovotest.data.entity.ParsedCode;
import ru.frosteye.urovotest.data.entity.Tag;
import ru.frosteye.urovotest.data.entity.TagContent;
import ru.frosteye.urovotest.data.entity.Tags;
import ru.frosteye.urovotest.data.repo.contract.NFCRepo;
import ru.frosteye.urovotest.data.repo.contract.SettingsRepo;
import ru.frosteye.urovotest.data.repo.contract.TagsRepo;

import static ru.frosteye.ovsa.data.storage.ResourceHelper.getString;

/**
 * Created by oleg on 17.10.17.
 */

@Singleton
public class CodeParser {

    private static final String TAG_HIGHLIGHT_COLOR_UNKNOWN = "#c40609";
    private static final String TAG_HIGHLIGHT_COLOR = "#088f08";

    private TagsRepo tagsRepo;
    private SettingsRepo settingsRepo;
    private NFCRepo nfcRepo;

    @Inject
    public CodeParser(TagsRepo tagsRepo, SettingsRepo settingsRepo,
                      NFCRepo nfcRepo) {
        this.tagsRepo = tagsRepo;
        this.settingsRepo = settingsRepo;
        this.nfcRepo = nfcRepo;
    }

    public ParsedCode createParsedCode(Code code) {
        return createParsedCode(code.getCodeValue(), code.getType());
    }

    public ParsedCode createParsedCode(String code, Code.Type type) {
        ParsedCode parsedCode = new ParsedCode();
        switch (type) {
            case EAN13:
                try {
                    parsedCode.addPart(getString(R.string.gcp), code.substring(0, 9));
                    parsedCode.addPart(getString(R.string.unit_number), code.substring(9, 12));
                    parsedCode.addPart(getString(R.string.control_number), code.substring(12));
                } catch (Exception e) {
                    parsedCode.addPart(getString(R.string.invalid_code), code);
                    e.printStackTrace();
                }
                break;
            case QR_CODE:
                try {
                    code = processTags(code, parsedCode);
                    if(code != null && !code.isEmpty()) {
                        parsedCode.addPart(getString(R.string.code), code);
                    }
                } catch (Exception e) {
                    parsedCode.addPart(getString(R.string.invalid_code), code);
                    e.printStackTrace();
                }
                break;
            case GTIN:
                try {
                    parsedCode.addPart(getString(R.string.leading_number), code.substring(0, 3));
                    parsedCode.addPart(getString(R.string.gcp), code.substring(3, 8));
                    parsedCode.addPart(getString(R.string.unit_number), code.substring(8, 13));
                    parsedCode.addPart(getString(R.string.control_number), code.substring(13));
                } catch (Exception e) {
                    parsedCode.addPart(getString(R.string.invalid_code), code);
                    e.printStackTrace();
                }
                break;
            case SSCC:
                try {
                    parsedCode.addPart(getString(R.string.container_type), code.substring(2, 3));
                    parsedCode.addPart(getString(R.string.gcp), code.substring(3, 12));
                    parsedCode.addPart(getString(R.string.container_number), code.substring(12, 19));
                    parsedCode.addPart(getString(R.string.control_number), code.substring(19));
                } catch (Exception e) {
                    parsedCode.addPart(getString(R.string.invalid_code), code);
                    e.printStackTrace();
                }
                break;
            case DATA_MATRIX:
                try {
                    code = processTags(code, parsedCode);
                    /*if(!code.startsWith("è") && !code.startsWith("\\?")) {
                        parsedCode.addPart(getString(R.string.code), code);
                    } else {

                    }*/
                    String[] parts = code.split("è");
                    if(parts.length == 1) parts = code.split("\\?");
                    if(parts.length == 1) parts = code.split("�");
                    parsedCode.addPart(getString(R.string.gtin), parts[1].substring(2, 16));
                    parsedCode.addPart(getString(R.string.series), parts[1].substring(18));

                    String expiration = new StringBuilder()
                            .append(parts[2].substring(2, 4)).append(".")
                            .append(parts[2].substring(4, 6)).append(".")
                            .append(parts[2].substring(6, 8)).toString();

                    parsedCode.addPart(getString(R.string.expiration), expiration);
                    parsedCode.addPart(getString(R.string.tnvd), parts[2].substring(11));
                    parsedCode.addPart(getString(R.string.serial_number), parts[3].substring(2));
                    parsedCode.addPart(getString(R.string.free_field), parts[4].substring(2));
                    parsedCode.setCodeTypePostfix(getString(R.string.gs1));

                } catch (Exception e) {
                    parsedCode.addPart(getString(R.string.code), code);
                    e.printStackTrace();
                }
                break;
        }

        return parsedCode;
    }

    private String processTags(String raw, ParsedCode parsedCode) {
        try {
            Tags tags = tagsRepo.load();
            AppSettings settings = settingsRepo.load();
            if(raw.contains("<")) {
                int startIndex = raw.indexOf("<");

                if(settings.isShowFoundTags()) {
                    int endIndex = raw.indexOf(">");
                    String tag = raw.substring(startIndex + 1, endIndex);

                    String content = raw.substring(endIndex + 1);
                    content = content.substring(0, content.indexOf("<"));

                    Tag parsedTag = tags.findTag(tag);
                    if(parsedTag != null) {
                        parsedCode.addPart(getString(R.string.tag_found), parsedTag.getTag(), TAG_HIGHLIGHT_COLOR);
                        parsedCode.addPart(getString(R.string.tag_found_value), String.format(parsedTag.getValue(), content), TAG_HIGHLIGHT_COLOR);

                    } else {
                        parsedCode.addPart(getString(R.string.tag_found_unknown), tag, TAG_HIGHLIGHT_COLOR_UNKNOWN);
                        parsedCode.addPart(getString(R.string.tag_found_value), content, TAG_HIGHLIGHT_COLOR_UNKNOWN);
                    }
                    parsedCode.addPart("", "");
                }


                String excluded = raw.substring(0, startIndex);
                return excluded;
            } else return raw;
        } catch (Exception e) {
            return raw;
        }
    }

    public TagContent extractTagContent(String raw) {
        try {
            Tags tags = tagsRepo.load();
            if(raw.contains("<")) {
                int startIndex = raw.indexOf("<");
                int endIndex = raw.indexOf(">");
                String tag = raw.substring(startIndex + 1, endIndex);

                String content = raw.substring(endIndex + 1);
                content = content.substring(0, content.indexOf("<"));
                Tag parsedTag = tags.findTag(tag);
                if(parsedTag == null) {
                    parsedTag = new Tag(tag, getString(R.string.pattern_unknown_tag));
                } else {
                    parsedTag.processNfc(nfcRepo.load());
                    parsedTag.processContent(content);
                }
                return new TagContent(parsedTag, content);
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
