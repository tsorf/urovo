package ru.frosteye.urovotest.app.environment;

/**
 * Created by oleg on 12.10.17.
 */

public class Keys {
    public static final String LOGIN = "login";
    public static final String PASSWORD = "password";
    public static final String CODE = "code";
    public static final String TYPE = "type";
    public static final String ACTION_UPDATE_CONNECTION_STATE = "UROVO.action.update.connection";
    public static final String MODULE = "module";
}
