package ru.frosteye.urovotest.app.environment;

import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;
import javax.inject.Singleton;

import ru.frosteye.urovotest.data.repo.contract.UserRepo;
import ru.frosteye.urovotest.presentation.view.impl.activity.BaseActivity;
import ru.frosteye.urovotest.presentation.view.impl.activity.LoginActivity;

/**
 * Created by oleg on 19.02.2018.
 */

@Singleton
public class LogoutManager {

    public static final long INACTIVE_IN = 1000 * 30;

    private BaseActivity activity;
    private Timer timer;
    private TimerTask task;
    private UserRepo userRepo;

    @Inject
    public LogoutManager(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    public void onResume(BaseActivity activity) {
        this.activity = activity;
        if (task != null) {
            task.cancel();
        }
    }

    public void onPause() {
        if(task != null) {
            task.cancel();
        }
        if(activity instanceof LoginActivity) return;
        task = new TimerTask() {
            @Override
            public void run() {
                if(activity != null) {
                    activity.runOnUiThread(() -> {
                        userRepo.save(null);
                        activity.startActivityAndClearTask(LoginActivity.class);
                        activity = null;
                    });
                }
            }
        };
        timer = new Timer(true);
        timer.schedule(task, INACTIVE_IN);
    }

    public void onDestroy() {
    }
}
