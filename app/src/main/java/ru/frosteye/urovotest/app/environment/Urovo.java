package ru.frosteye.urovotest.app.environment;

import android.app.Application;
import android.support.multidex.MultiDexApplication;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
import ru.frosteye.urovotest.app.di.component.AppComponent;
import ru.frosteye.urovotest.app.di.component.DaggerAppComponent;
import ru.frosteye.urovotest.app.di.module.AppModule;


public class Urovo extends MultiDexApplication {

    private static AppComponent appComponent;

    public static AppComponent getAppComponent() {
        return appComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }
}
