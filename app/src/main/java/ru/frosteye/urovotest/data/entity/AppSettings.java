package ru.frosteye.urovotest.data.entity;

/**
 * Created by oleg on 17.10.17.
 */

public class AppSettings {

    public enum ConnectionType {
        REST, WEBSERVICE
    }

    private boolean sleep = false;
    private boolean scannerAutoTurnOff = true;
    private boolean beep = true;
    private boolean showFoundTags = true;
    private String serverUrl;
    private ConnectionType connectionType = ConnectionType.REST;

    public boolean shouldSleep() {
        return sleep;
    }

    public AppSettings setSleep(boolean sleep) {
        this.sleep = sleep;
        return this;
    }

    public boolean isSleep() {
        return sleep;
    }

    public boolean isScannerAutoTurnOff() {
        return scannerAutoTurnOff;
    }

    public AppSettings setScannerAutoTurnOff(boolean scannerAutoTurnOff) {
        this.scannerAutoTurnOff = scannerAutoTurnOff;
        return this;
    }

    public boolean isShowFoundTags() {
        return showFoundTags;
    }

    public AppSettings setShowFoundTags(boolean showFoundTags) {
        this.showFoundTags = showFoundTags;
        return this;
    }

    public boolean isBeep() {
        return beep;
    }

    public AppSettings setBeep(boolean beep) {
        this.beep = beep;
        return this;
    }

    public String getServerUrl() {
        return serverUrl;
    }

    public AppSettings setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
        return this;
    }

    public ConnectionType getConnectionType() {
        return connectionType;
    }

    public AppSettings setConnectionType(
            ConnectionType connectionType) {
        this.connectionType = connectionType;
        return this;
    }
}
