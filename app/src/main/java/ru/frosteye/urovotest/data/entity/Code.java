package ru.frosteye.urovotest.data.entity;

import java.util.Date;

import ru.frosteye.ovsa.tool.DateTools;
import ru.frosteye.urovotest.R;

/**
 * Created by oleg on 12.10.17.
 */

public class Code {

    public enum Type {
        EAN13(R.drawable.ic_barcode),
        DATA_MATRIX(R.drawable.ic_datamatrix),
        SSCC(R.drawable.ic_barcode),
        GTIN(R.drawable.ic_barcode),
        QR_CODE(R.drawable.ic_qr);

        private int icon;

        Type(int icon) {
            this.icon = icon;
        }

        public int getIcon() {
            return icon;
        }
    }

    private int codeId;
    private int scanId;
    private String codeValue;
    private String codeType;
    private String codeContents;
    private Date scannedAt;
    private Date createdAt;
    private String requestResult;
    private Type type;
    private boolean localCode;

    private boolean fresh;

    public boolean isFresh() {
        return fresh;
    }

    public Code setFresh(boolean fresh) {
        this.fresh = fresh;
        return this;
    }

    public String getRequestResult() {
        return requestResult;
    }

    public Code setRequestResult(String requestResult) {
        this.requestResult = requestResult;
        return this;
    }

    public boolean isLocalCode() {
        return localCode;
    }

    public Code setLocalCode(boolean localCode) {
        this.localCode = localCode;
        return this;
    }

    public int getScanId() {
        return scanId;
    }

    public Code setCodeContents(String codeContents) {
        this.codeContents = codeContents;
        return this;
    }

    public Code setScannedAt(Date scannedAt) {
        this.scannedAt = scannedAt;
        return this;
    }

    public Date getScannedAt() {
        return scannedAt;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public int getCodeId() {
        return codeId;
    }

    public String getCodeValue() {
        return codeValue;
    }

    public Code setCodeValue(String codeValue) {
        this.codeValue = codeValue;
        return this;
    }

    public Code setCodeType(String codeType) {
        this.codeType = codeType;
        return this;
    }

    public Type getType() {
        try {
            return Type.valueOf(codeType);
        } catch (Exception e) {
            return Type.EAN13;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Code code = (Code) o;

        if (!codeValue.equals(code.codeValue)) return false;
        if (!codeType.equals(code.codeType)) return false;
        return DateTools.formatDottedDateWithTimeWithSeconds(scannedAt)
                .equals(DateTools.formatDottedDateWithTimeWithSeconds(code.scannedAt));
    }

    @Override
    public int hashCode() {
        int result = codeValue.hashCode();
        result = 31 * result + codeType.hashCode();
        result = 31 * result + scannedAt.hashCode();
        return result;
    }

    public String getCodeContents() {
        return codeContents;
    }
}
