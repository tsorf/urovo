package ru.frosteye.urovotest.data.entity;

import java.util.ArrayList;
import java.util.List;

import eu.davidea.flexibleadapter.items.IFlexible;
import ru.frosteye.urovotest.execution.operation.Operation;

/**
 * Created by oleg on 18.02.2018.
 */

public class CodeScan {

    private Code code;
    private boolean completed;
    private List<Operation> operations = new ArrayList<>();

    private transient boolean selected;

    public CodeScan(Code code) {
        this.code = code;
    }

    public List<Operation> getOperations() {
        return operations;
    }

    public List<IFlexible> createAdapterItems() {
        List<IFlexible> iFlexibles = new ArrayList<>();
        for(Operation operation: operations) {
            iFlexibles.add(operation.createAdapterItem());
        }
        return iFlexibles;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public Code getCode() {
        return code;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }
}
