package ru.frosteye.urovotest.data.entity;

/**
 * Created by oleg on 05.12.2017.
 */

public class NFCEntity {
    private String rfarm;
    private String rfuser;
    private String rfoperation;
    private String rfserial;

    public String getRfarm() {
        return rfarm;
    }

    public String getRfuser() {
        return rfuser;
    }

    public String getRfoperation() {
        return rfoperation;
    }

    public String getRfserial() {
        return rfserial;
    }

    public NFCEntity setRfarm(String rfarm) {
        this.rfarm = rfarm;
        return this;
    }

    public NFCEntity setRfuser(String rfuser) {
        this.rfuser = rfuser;
        return this;
    }

    public NFCEntity setRfoperation(String rfoperation) {
        this.rfoperation = rfoperation;
        return this;
    }

    public NFCEntity setRfserial(String rfserial) {
        this.rfserial = rfserial;
        return this;
    }
}
