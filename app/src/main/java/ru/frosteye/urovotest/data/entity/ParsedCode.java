package ru.frosteye.urovotest.data.entity;

import android.graphics.Color;
import android.support.v4.util.Pair;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by oleg on 17.10.17.
 */

public class ParsedCode {
    private static final String DEFAULT_COLOR = "#000000";

    private final Map<String, Pair<String, String>> parts = new LinkedHashMap<>();
    private String codeTypePostfix = "";

    public ParsedCode addPart(String key, String value) {
        addPart(key, value, null);
        return this;
    }

    public ParsedCode addPart(String key, String value, String color) {
        parts.put(key, new Pair<>(color == null ? DEFAULT_COLOR : color, value));
        return this;
    }

    public String getCodeTypePostfix() {
        return codeTypePostfix;
    }

    public ParsedCode setCodeTypePostfix(String codeTypePostfix) {
        this.codeTypePostfix = codeTypePostfix;
        return this;
    }

    public String format() {
        StringBuilder builder = new StringBuilder();
        for(Map.Entry<String, Pair<String, String>> entry: parts.entrySet()) {
            builder.append("<b><font color=\"")
                    .append(entry.getValue().first)
                    .append("\">")
                    .append(entry.getKey())
                    .append("</font></b> ")
                    .append(entry.getValue().second)
                    .append("<br>");
        }
        return builder.toString();
    }
}
