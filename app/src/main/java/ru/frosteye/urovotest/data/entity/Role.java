package ru.frosteye.urovotest.data.entity;

import java.util.List;

/**
 * Created by oleg on 02.02.2018.
 */

public class Role {
    private int id;
    private String name;
    private String description;
    private List<Permission> permissions;
    private List<UiModule> modules;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public List<Permission> getPermissions() {
        return permissions;
    }

    public List<UiModule> getModules() {
        return modules;
    }

    public static class Permission {
        private int id;
        private String name;
        private String description;

        public int getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public String getDescription() {
            return description;
        }
    }
}
