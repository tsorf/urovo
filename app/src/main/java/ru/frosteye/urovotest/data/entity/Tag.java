package ru.frosteye.urovotest.data.entity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

/**
 * Created by oleg on 25.10.17.
 */
@Entity
public class Tag {

    @Id
    private int id;
    private String value;
    private String tag;
    private String url;
    private String method;
    private String params;
    private transient String parsedParams;


    public Tag(String tag, String value) {
        this.tag = tag;
        this.value = value;
    }

    @Generated(hash = 475565472)
    public Tag(int id, String value, String tag, String url, String method,
            String params) {
        this.id = id;
        this.value = value;
        this.tag = tag;
        this.url = url;
        this.method = method;
        this.params = params;
    }
    @Generated(hash = 1605720318)
    public Tag() {
    }
    public int getId() {
        return this.id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getValue() {
        return this.value;
    }
    public void setValue(String value) {
        this.value = value;
    }
    public String getTag() {
        return this.tag;
    }
    public void setTag(String tag) {
        this.tag = tag;
    }
    public String getUrl() {
        return this.url;
    }
    public String getFullUrl() {
        StringBuilder builder = new StringBuilder();
        if(url == null || url.isEmpty()) return null;
        builder.append(url);
        if(parsedParams != null) {
            builder.append("?").append(parsedParams);
        } else {
            builder.append("?").append(params);
        }
        return builder.toString();
    }
    public void setUrl(String url) {
        this.url = url;
    }
    public String getMethod() {
        return this.method;
    }
    public void setMethod(String method) {
        this.method = method;
    }
    public String getParams() {
        return this.params;
    }
    public void setParams(String params) {
        this.params = params;
    }

    public void processNfc(NFCEntity entity) {
        if(params == null) return;
        parsedParams = params.replace("%rfarm", entity.getRfarm());
        parsedParams = parsedParams.replace("%rfoperation", entity.getRfoperation());
        parsedParams = parsedParams.replace("%rfuser", entity.getRfuser());
        parsedParams = parsedParams.replace("%rfserial", entity.getRfserial());
    }

    public void processContent(String content) {
        if(parsedParams == null) return;
        parsedParams = parsedParams.replace("%s", content);
    }

    public String getParsedParams() {
        return parsedParams;
    }
}
