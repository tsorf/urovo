package ru.frosteye.urovotest.data.entity;

import ru.frosteye.ovsa.execution.network.request.RequestParams;

/**
 * Created by oleg on 16.11.2017.
 */

public class TagContent {

    public enum RequestType {
        POST, GET, SOAP
    }

    private Tag tag;
    private String content;

    public TagContent(Tag tag, String content) {
        this.tag = tag;

        this.content = content;
    }

    public Tag getTag() {
        return tag;
    }

    public String getContent() {
        return content;
    }

    public RequestType getRequestType() {
        try {
            return RequestType.valueOf(tag.getMethod());
        } catch (Exception e) {
            return RequestType.GET;
        }
    }

    public String getUrl() {
        try {
            return tag.getUrl();
        } catch (Exception e) {
            return null;
        }
    }

    public RequestParams getParams() {
        try {
            return new RequestParams(tag.getParsedParams());
        } catch (Exception e) {
            return new RequestParams();
        }
    }
}
