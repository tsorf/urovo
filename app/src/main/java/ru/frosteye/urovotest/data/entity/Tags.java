package ru.frosteye.urovotest.data.entity;

import java.util.List;

/**
 * Created by oleg on 25.10.17.
 */

public class Tags {
    private List<Tag> items;

    public Tags(List<Tag> items) {
        this.items = items;
    }

    public List<Tag> getItems() {
        return items;
    }

    public Tag findTag(String tag) {
        for(Tag parsedTag: items) {
            if(parsedTag.getTag().equals(tag))
                return parsedTag;
        }
        return null;
    }
}
