package ru.frosteye.urovotest.data.entity;

import java.util.Date;

/**
 * Created by oleg on 02.02.2018.
 */

public class Token {
    private String value;
    private Date createdAt;

    public String getValue() {
        return value;
    }

    public Date getCreatedAt() {
        return createdAt;
    }
}
