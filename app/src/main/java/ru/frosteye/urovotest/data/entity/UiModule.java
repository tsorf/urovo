package ru.frosteye.urovotest.data.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import ru.frosteye.urovotest.execution.operation.Operation;

/**
 * Created by oleg on 09.02.2018.
 */

public class UiModule implements Serializable {
    private int id;
    private String name;
    private String description;
    private String icon;
    private int sortOrder;
    private Date createdAt;

    private List<Operation> operations;

    public List<Operation> getOperations() {
        return operations;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getIcon() {
        return icon;
    }

    public int getSortOrder() {
        return sortOrder;
    }

    public Date getCreatedAt() {
        return createdAt;
    }
}
