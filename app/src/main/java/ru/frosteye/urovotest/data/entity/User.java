package ru.frosteye.urovotest.data.entity;

import java.util.Date;

/**
 * Created by oleg on 12.10.17.
 */

public class User {
    private int id;
    private String name;
    private String login;
    private Date createdAt;
    private Role role;
    private String firstName;
    private String lastName;

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFormattedName() {
        if(firstName == null || firstName.isEmpty()
                || lastName == null || lastName.isEmpty()) {
            return login;
        }
        return String.format("%s %s", firstName, lastName);
    }

    public Role getRole() {
        return role;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLogin() {
        return login;
    }

    public Date getCreatedAt() {
        return createdAt;
    }
}
