package ru.frosteye.urovotest.data.entity.container;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import ru.frosteye.urovotest.data.entity.Code;

/**
 * Created by oleg on 13.11.2017.
 */

public class Checks {
    private List<Code> items = new ArrayList<>();
    private transient List<CodeInfo> adapterItems;
    private List<Code> local = new ArrayList<>();

    public Checks() {
    }

    public List<Code> getLocal() {
        return local;
    }

    public Checks setLocal(List<Code> local) {
        this.local = local;
        return this;
    }

    public Checks fillRemoteItems(List<CodeInfo> adapterItems) {
        this.adapterItems = adapterItems;
        items.clear();
        for(CodeInfo codeInfo: adapterItems) {
            items.add(codeInfo.getModel());
        }
        createAdapterItems();
        return this;
    }

    public List<Code> getItems() {
        return items;
    }

    public List<CodeInfo> getAdapterItems() {
        if(adapterItems == null) {
            createAdapterItems();
        }
        return adapterItems;
    }

    public void createAdapterItems() {
        adapterItems = new ArrayList<>();
        for(Code code: items) {
            adapterItems.add(new CodeInfo(code));
        }
        for(Code code: local) {
            adapterItems.add(new CodeInfo(code));
        }
        Collections.sort(adapterItems);
    }
}
