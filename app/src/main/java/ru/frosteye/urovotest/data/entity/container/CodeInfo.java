package ru.frosteye.urovotest.data.entity.container;

import android.support.annotation.NonNull;

import ru.frosteye.ovsa.presentation.adapter.AdapterItem;
import ru.frosteye.urovotest.R;
import ru.frosteye.urovotest.data.entity.Code;
import ru.frosteye.urovotest.presentation.view.impl.widget.CodeView;

/**
 * Created by oleg on 12.10.17.
 */

public class CodeInfo extends AdapterItem<Code, CodeView> implements Comparable<CodeInfo> {


    public CodeInfo() {
    }

    public CodeInfo(Code model) {
        super(model);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.view_code;
    }

    @Override
    public int compareTo(@NonNull CodeInfo o) {
        return getModel().getScannedAt().compareTo(o.getModel().getScannedAt());
    }
}
