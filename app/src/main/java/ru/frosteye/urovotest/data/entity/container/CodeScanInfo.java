package ru.frosteye.urovotest.data.entity.container;

import android.support.annotation.NonNull;

import ru.frosteye.ovsa.presentation.adapter.AdapterItem;
import ru.frosteye.urovotest.R;
import ru.frosteye.urovotest.data.entity.CodeScan;
import ru.frosteye.urovotest.data.entity.UiModule;
import ru.frosteye.urovotest.presentation.view.impl.widget.CodeRowView;
import ru.frosteye.urovotest.presentation.view.impl.widget.ModuleView;

/**
 * Created by oleg on 12.10.17.
 */

public class CodeScanInfo extends AdapterItem<CodeScan, CodeRowView>   {


    public CodeScanInfo() {
    }

    public CodeScanInfo(CodeScan model) {
        super(model);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.view_code_row;
    }
}
