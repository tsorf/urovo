package ru.frosteye.urovotest.data.entity.container;

import android.support.annotation.NonNull;

import ru.frosteye.ovsa.presentation.adapter.AdapterItem;
import ru.frosteye.urovotest.R;
import ru.frosteye.urovotest.data.entity.Code;
import ru.frosteye.urovotest.data.entity.Tag;
import ru.frosteye.urovotest.presentation.view.impl.widget.CodeView;
import ru.frosteye.urovotest.presentation.view.impl.widget.TagView;

/**
 * Created by oleg on 12.10.17.
 */

public class TagInfo extends AdapterItem<Tag, TagView>  {


    public TagInfo() {
    }

    public TagInfo(Tag model) {
        super(model);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.view_tag;
    }
}
