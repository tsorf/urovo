package ru.frosteye.urovotest.data.entity.container;

import android.support.annotation.NonNull;

import ru.frosteye.ovsa.presentation.adapter.AdapterItem;
import ru.frosteye.urovotest.R;
import ru.frosteye.urovotest.data.entity.Tag;
import ru.frosteye.urovotest.data.entity.UiModule;
import ru.frosteye.urovotest.presentation.view.impl.widget.ModuleView;
import ru.frosteye.urovotest.presentation.view.impl.widget.TagView;

/**
 * Created by oleg on 12.10.17.
 */

public class UiModuleInfo extends AdapterItem<UiModule, ModuleView> implements Comparable<UiModuleInfo>  {


    public UiModuleInfo() {
    }

    public UiModuleInfo(UiModule model) {
        super(model);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.view_module;
    }

    @Override
    public int compareTo(@NonNull UiModuleInfo uiModuleInfo) {
        return getModel().getSortOrder() > uiModuleInfo.getModel().getSortOrder() ? 1 : -1;
    }
}
