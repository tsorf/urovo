package ru.frosteye.urovotest.data.entity.container.operation;

import ru.frosteye.ovsa.presentation.adapter.AdapterItem;
import ru.frosteye.urovotest.R;
import ru.frosteye.urovotest.execution.operation.Operation;
import ru.frosteye.urovotest.execution.operation.VideoOperation;
import ru.frosteye.urovotest.presentation.view.impl.widget.operation.DefaultOperationView;
import ru.frosteye.urovotest.presentation.view.impl.widget.operation.VideoOperationView;

/**
 * Created by oleg on 18.02.2018.
 */

public class DefaultOperationInfo extends AdapterItem<Operation, DefaultOperationView> {

    public DefaultOperationInfo(Operation model) {
        super(model);
    }

    public DefaultOperationInfo() {
    }

    @Override
    public int getLayoutRes() {
        return R.layout.view_operation_default;
    }
}
