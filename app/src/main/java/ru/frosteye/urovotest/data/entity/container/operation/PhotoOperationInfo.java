package ru.frosteye.urovotest.data.entity.container.operation;

import ru.frosteye.ovsa.presentation.adapter.AdapterItem;
import ru.frosteye.urovotest.R;
import ru.frosteye.urovotest.execution.operation.PhotoOperation;
import ru.frosteye.urovotest.presentation.view.impl.widget.operation.PhotoOperationView;

/**
 * Created by oleg on 18.02.2018.
 */

public class PhotoOperationInfo extends AdapterItem<PhotoOperation, PhotoOperationView> {

    public PhotoOperationInfo(PhotoOperation model) {
        super(model);
    }

    public PhotoOperationInfo() {
    }

    @Override
    public int getLayoutRes() {
        return R.layout.view_operation_photo;
    }
}
