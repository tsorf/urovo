package ru.frosteye.urovotest.data.entity.container.operation;

import ru.frosteye.ovsa.presentation.adapter.AdapterItem;
import ru.frosteye.urovotest.R;
import ru.frosteye.urovotest.execution.operation.PhotoOperation;
import ru.frosteye.urovotest.execution.operation.VideoOperation;
import ru.frosteye.urovotest.presentation.view.impl.widget.operation.PhotoOperationView;
import ru.frosteye.urovotest.presentation.view.impl.widget.operation.VideoOperationView;

/**
 * Created by oleg on 18.02.2018.
 */

public class VIdeoOperationInfo extends AdapterItem<VideoOperation, VideoOperationView> {

    public VIdeoOperationInfo(VideoOperation model) {
        super(model);
    }

    public VIdeoOperationInfo() {
    }

    @Override
    public int getLayoutRes() {
        return R.layout.view_operation_video;
    }
}
