package ru.frosteye.urovotest.data.repo.contract;

import java.util.List;

import ru.frosteye.ovsa.data.storage.Repo;
import ru.frosteye.urovotest.data.entity.Code;
import ru.frosteye.urovotest.data.entity.container.Checks;
import ru.frosteye.urovotest.data.entity.container.CodeInfo;

/**
 * Created by oleg on 13.11.2017.
 */

public interface CheckRepo extends Repo<Checks> {
}
