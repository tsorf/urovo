package ru.frosteye.urovotest.data.repo.contract;

import ru.frosteye.ovsa.data.storage.Repo;
import ru.frosteye.urovotest.data.entity.AppSettings;

/**
 * Created by oleg on 17.10.17.
 */

public interface SettingsRepo extends Repo<AppSettings> {
}
