package ru.frosteye.urovotest.data.repo.contract;

import ru.frosteye.ovsa.data.storage.Repo;
import ru.frosteye.urovotest.data.entity.Tags;

/**
 * Created by oleg on 25.10.17.
 */

public interface TagsRepo extends Repo<Tags> {
}
