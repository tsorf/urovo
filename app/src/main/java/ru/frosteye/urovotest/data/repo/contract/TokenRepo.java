package ru.frosteye.urovotest.data.repo.contract;

import ru.frosteye.ovsa.data.storage.Repo;
import ru.frosteye.ovsa.execution.network.client.IdentityProvider;
import ru.frosteye.urovotest.data.entity.Token;

/**
 * Created by oleg on 02.02.2018.
 */

public interface TokenRepo extends Repo<Token>, IdentityProvider {
}
