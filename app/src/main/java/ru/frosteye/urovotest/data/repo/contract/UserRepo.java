package ru.frosteye.urovotest.data.repo.contract;

import ru.frosteye.ovsa.data.storage.Repo;
import ru.frosteye.ovsa.execution.network.client.IdentityProvider;
import ru.frosteye.urovotest.data.entity.User;

/**
 * Created by oleg on 12.10.17.
 */

public interface UserRepo extends Repo<User> {
}
