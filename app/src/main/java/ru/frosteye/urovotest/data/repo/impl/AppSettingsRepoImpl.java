package ru.frosteye.urovotest.data.repo.impl;

import javax.inject.Inject;

import ru.frosteye.ovsa.data.storage.BaseRepo;
import ru.frosteye.ovsa.data.storage.ResourceHelper;
import ru.frosteye.ovsa.data.storage.Storage;
import ru.frosteye.urovotest.R;
import ru.frosteye.urovotest.data.entity.AppSettings;
import ru.frosteye.urovotest.data.repo.contract.SettingsRepo;

/**
 * Created by oleg on 17.10.17.
 */

public class AppSettingsRepoImpl extends BaseRepo<AppSettings> implements SettingsRepo {

    @Inject
    public AppSettingsRepoImpl(Storage storage) {
        super(storage);
    }

    @Override
    protected Class<AppSettings> provideClass() {
        return AppSettings.class;
    }

    @Override
    protected AppSettings handleNull() {
        AppSettings settings = new AppSettings();
        settings.setServerUrl(ResourceHelper.getString(R.string.config_api_url));
        return settings;
    }
}
