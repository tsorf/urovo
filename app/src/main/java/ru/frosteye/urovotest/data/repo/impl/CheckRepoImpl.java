package ru.frosteye.urovotest.data.repo.impl;

import java.util.List;

import javax.inject.Inject;

import ru.frosteye.ovsa.data.storage.BaseRepo;
import ru.frosteye.ovsa.data.storage.Storage;
import ru.frosteye.urovotest.data.entity.container.Checks;
import ru.frosteye.urovotest.data.entity.container.CodeInfo;
import ru.frosteye.urovotest.data.repo.contract.CheckRepo;

/**
 * Created by oleg on 13.11.2017.
 */

public class CheckRepoImpl extends BaseRepo<Checks> implements CheckRepo {

    @Inject
    public CheckRepoImpl(Storage storage) {
        super(storage);
    }

    @Override
    protected Class<Checks> provideClass() {
        return Checks.class;
    }

    @Override
    protected Checks handleNull() {
        return new Checks();
    }
}
