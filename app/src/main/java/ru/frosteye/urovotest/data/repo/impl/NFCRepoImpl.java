package ru.frosteye.urovotest.data.repo.impl;

import javax.inject.Inject;

import ru.frosteye.ovsa.data.storage.BaseRepo;
import ru.frosteye.ovsa.data.storage.Storage;
import ru.frosteye.ovsa.execution.network.client.IdentityProvider;
import ru.frosteye.urovotest.data.entity.NFCEntity;
import ru.frosteye.urovotest.data.entity.User;
import ru.frosteye.urovotest.data.repo.contract.NFCRepo;
import ru.frosteye.urovotest.data.repo.contract.UserRepo;

/**
 * Created by oleg on 12.10.17.
 */

public class NFCRepoImpl extends BaseRepo<NFCEntity> implements NFCRepo {

    @Inject
    public NFCRepoImpl(Storage storage) {
        super(storage);
    }

    @Override
    protected Class<NFCEntity> provideClass() {
        return NFCEntity.class;
    }

    @Override
    protected NFCEntity handleNull() {
        NFCEntity entity = new NFCEntity();
        entity.setRfarm("rfarm");
        entity.setRfoperation("rfoperation");
        entity.setRfserial("rfserial");
        entity.setRfuser("rfuser");
        return entity;
    }
}
