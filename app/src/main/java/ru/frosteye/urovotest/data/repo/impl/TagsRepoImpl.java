package ru.frosteye.urovotest.data.repo.impl;

import java.util.ArrayList;

import javax.inject.Inject;

import ru.frosteye.ovsa.data.storage.BaseRepo;
import ru.frosteye.ovsa.data.storage.Storage;
import ru.frosteye.urovotest.data.entity.Tags;
import ru.frosteye.urovotest.data.repo.contract.TagsRepo;

/**
 * Created by oleg on 25.10.17.
 */

public class TagsRepoImpl extends BaseRepo<Tags> implements TagsRepo {

    @Inject
    public TagsRepoImpl(Storage storage) {
        super(storage);
    }

    @Override
    protected Class<Tags> provideClass() {
        return Tags.class;
    }

    @Override
    protected Tags handleNull() {
        return new Tags(new ArrayList<>());
    }
}
