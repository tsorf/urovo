package ru.frosteye.urovotest.data.repo.impl;

import ru.frosteye.ovsa.data.storage.Repo;
import ru.frosteye.urovotest.data.entity.Tags;
import ru.frosteye.urovotest.data.repo.contract.TagsRepo;

/**
 * Created by oleg on 16.11.2017.
 */

public class TagsRepoSqliteImpl implements TagsRepo {
    @Override
    public Tags load() {
        return null;
    }

    @Override
    public void save(Tags tags) {

    }

    @Override
    public void executeAndSave(RepoRunnable<Tags> runnable) {
        Tags tags = load();
        runnable.run(tags);
        save(tags);
    }
}
