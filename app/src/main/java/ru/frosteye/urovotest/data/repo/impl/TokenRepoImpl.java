package ru.frosteye.urovotest.data.repo.impl;

import javax.inject.Inject;

import ru.frosteye.ovsa.data.storage.BaseRepo;
import ru.frosteye.ovsa.data.storage.Storage;
import ru.frosteye.urovotest.data.entity.Token;
import ru.frosteye.urovotest.data.repo.contract.TokenRepo;

/**
 * Created by oleg on 02.02.2018.
 */

public class TokenRepoImpl extends BaseRepo<Token> implements TokenRepo {

    @Inject
    public TokenRepoImpl(Storage storage) {
        super(storage);
    }

    @Override
    public String provideIdentity() {
        Token token = load();
        if(token != null) return token.getValue();
        return null;
    }

    @Override
    protected Class<Token> provideClass() {
        return Token.class;
    }
}
