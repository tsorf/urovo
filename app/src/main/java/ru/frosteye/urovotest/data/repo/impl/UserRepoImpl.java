package ru.frosteye.urovotest.data.repo.impl;

import javax.inject.Inject;

import ru.frosteye.ovsa.data.storage.BaseRepo;
import ru.frosteye.ovsa.data.storage.Storage;
import ru.frosteye.ovsa.execution.network.client.IdentityProvider;
import ru.frosteye.urovotest.data.entity.User;
import ru.frosteye.urovotest.data.repo.contract.UserRepo;

/**
 * Created by oleg on 12.10.17.
 */

public class UserRepoImpl extends BaseRepo<User> implements UserRepo {

    @Inject
    public UserRepoImpl(Storage storage) {
        super(storage);
    }

    @Override
    protected Class<User> provideClass() {
        return User.class;
    }
}
