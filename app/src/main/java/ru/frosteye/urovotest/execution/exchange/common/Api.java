package ru.frosteye.urovotest.execution.exchange.common;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;
import ru.frosteye.ovsa.execution.network.request.RequestParams;
import ru.frosteye.ovsa.execution.network.response.MessageResponse;
import ru.frosteye.urovotest.data.entity.Code;
import ru.frosteye.urovotest.data.entity.Tag;
import ru.frosteye.urovotest.data.entity.User;
import ru.frosteye.urovotest.data.entity.container.CodeInfo;
import ru.frosteye.urovotest.execution.exchange.request.LogRequest;
import ru.frosteye.urovotest.execution.exchange.request.LoginRequest;
import ru.frosteye.urovotest.execution.exchange.request.ReportRequest;
import ru.frosteye.urovotest.execution.exchange.request.ScanRequest;
import ru.frosteye.urovotest.execution.exchange.response.LoginResponse;
import ru.frosteye.urovotest.execution.exchange.response.ResultResponse;

/**
 * Created by oleg on 12.10.17.
 */

public interface Api {

    @POST("user/auth")
    Call<LoginResponse> auth(@Body LoginRequest request);

    @GET("user/profile")
    Call<User> profile();

    @GET("codes/")
    Call<List<CodeInfo>> list();

    @GET("tags/")
    Call<List<Tag>> listTags();

    @GET("ping")
    Call<MessageResponse> ping();

    @POST("log")
    Call<MessageResponse> log(@Body LogRequest logRequest);

    @POST("scan/")
    Call<CodeInfo> scan(@Body ScanRequest request);

    @POST("scan/report/")
    Call<CodeInfo> report(@Body ReportRequest request);

    @POST("sync/")
    Call<List<Code>> sync(@Body List<Code> request);

    @POST
    @FormUrlEncoded
    Call<String> tagPostRequest(@Url String url, @FieldMap RequestParams params);

    @GET
    Call<String> tagGetRequest(@Url String url, @QueryMap RequestParams params);
}
