package ru.frosteye.urovotest.execution.exchange.common;

/**
 * Created by oleg on 12.10.17.
 */

public interface ApiClient {
    Api getApi();
    void dropService();
}
