package ru.frosteye.urovotest.execution.exchange.common;

import com.google.gson.GsonBuilder;

import javax.inject.Inject;

import retrofit2.Retrofit;
import ru.frosteye.ovsa.di.qualifer.ApiUrl;
import ru.frosteye.ovsa.execution.network.client.BaseRetrofitClient;
import ru.frosteye.ovsa.execution.network.client.IdentityProvider;
import ru.frosteye.urovotest.data.entity.container.CodeInfo;
import ru.frosteye.urovotest.data.repo.contract.SettingsRepo;
import ru.frosteye.urovotest.execution.exchange.request.gson.CodeInfoDeserializer;

/**
 * Created by oleg on 12.10.17.
 */

public class RetrofitApiClient extends BaseRetrofitClient<Api> implements ApiClient {

    private SettingsRepo repo;

    @Inject
    public RetrofitApiClient(@ApiUrl String baseUrl,
                             IdentityProvider identityProvider, SettingsRepo repo) {
        super(baseUrl, identityProvider);
        this.repo = repo;
    }

    @Override
    protected GsonBuilder createGsonBuilder() {
        GsonBuilder builder = super.createGsonBuilder();
        builder.registerTypeAdapter(CodeInfo.class, new CodeInfoDeserializer());
        return builder;
    }

    @Override
    protected void populateRetrofitBuilder(Retrofit.Builder builder) {
    }

    @Override
    protected int getConnectTimeout() {
        return 10;
    }

    @Override
    protected int getReadTimeout() {
        return 10;
    }

    @Override
    public Class<Api> apiClass() {
        return Api.class;
    }

    @Override
    public void dropService() {
        init(repo.load().getServerUrl(), getIdentityProvider());
    }

    @Override
    protected String getAuthHeaderValuePrefix() {
        return "Bearer ";
    }
}
