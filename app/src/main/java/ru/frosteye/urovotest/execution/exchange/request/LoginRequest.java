package ru.frosteye.urovotest.execution.exchange.request;

/**
 * Created by oleg on 12.10.17.
 */

public class LoginRequest {
    private String login, password;

    public LoginRequest(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }
}
