package ru.frosteye.urovotest.execution.exchange.request;

/**
 * Created by oleg on 16.11.2017.
 */

public class ReportRequest {
    private int scanId;
    private String requestResult;

    public ReportRequest(int scanId, String requestResult) {
        this.scanId = scanId;
        this.requestResult = requestResult;
    }
}
