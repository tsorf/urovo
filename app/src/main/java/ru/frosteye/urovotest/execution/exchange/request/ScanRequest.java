package ru.frosteye.urovotest.execution.exchange.request;

import ru.frosteye.urovotest.data.entity.UiModule;

/**
 * Created by oleg on 12.10.17.
 */

public class ScanRequest {
    private String codeValue;
    private String codeType;
    private UiModule module;

    public ScanRequest(String codeValue, String codeType,
                       UiModule module) {
        this.codeValue = codeValue;
        this.codeType = codeType;
        this.module = module;
    }

    public UiModule getModule() {
        return module;
    }

    public String getCodeValue() {
        return codeValue;
    }

    public String getCodeType() {
        return codeType;
    }
}
