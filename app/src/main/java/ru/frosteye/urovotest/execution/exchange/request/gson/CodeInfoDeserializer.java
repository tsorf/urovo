package ru.frosteye.urovotest.execution.exchange.request.gson;

import ru.frosteye.ovsa.execution.serialization.AdapterItemDeserializer;
import ru.frosteye.urovotest.data.entity.Code;
import ru.frosteye.urovotest.data.entity.container.CodeInfo;

/**
 * Created by oleg on 12.10.17.
 */

public class CodeInfoDeserializer extends AdapterItemDeserializer<Code, CodeInfo> {
    @Override
    protected Class<Code> provideType() {
        return Code.class;
    }

    @Override
    protected Class<CodeInfo> provideWrapperType() {
        return CodeInfo.class;
    }
}
