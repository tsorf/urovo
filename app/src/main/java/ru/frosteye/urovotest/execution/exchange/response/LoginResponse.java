package ru.frosteye.urovotest.execution.exchange.response;

import java.util.List;

import ru.frosteye.urovotest.data.entity.Tag;
import ru.frosteye.urovotest.data.entity.Token;
import ru.frosteye.urovotest.data.entity.User;

/**
 * Created by oleg on 25.10.17.
 */

public class LoginResponse {
    private User user;
    private Token token;

    public User getUser() {
        return user;
    }

    public Token getToken() {
        return token;
    }
}
