package ru.frosteye.urovotest.execution.exchange.response;

/**
 * Created by oleg on 16.11.2017.
 */

public class ResultResponse {
    private String result;

    public String getResult() {
        return result;
    }

    @Override
    public String toString() {
        return "Result: " + result;
    }
}
