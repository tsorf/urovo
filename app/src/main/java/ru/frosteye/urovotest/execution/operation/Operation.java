package ru.frosteye.urovotest.execution.operation;

import java.io.Serializable;

import eu.davidea.flexibleadapter.items.IFlexible;
import ru.frosteye.urovotest.data.entity.container.operation.DefaultOperationInfo;

/**
 * Created by oleg on 18.02.2018.
 */

public class Operation implements Serializable {

    public static final int PHOTO = 2;
    public static final int VIDEO = 3;

    private int id;
    private String name;
    private String description;

    public Operation() {
    }

    public Operation(Operation operation) {
        this.name = operation.name;
        this.description = operation.description;
    }

    public Operation(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public boolean isCompleted() {
        return true;
    }

    public IFlexible createAdapterItem() {
        return new DefaultOperationInfo(this);
    }
}
