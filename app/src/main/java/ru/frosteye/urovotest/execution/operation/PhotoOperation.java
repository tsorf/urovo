package ru.frosteye.urovotest.execution.operation;

import java.io.File;

import eu.davidea.flexibleadapter.items.IFlexible;
import ru.frosteye.urovotest.data.entity.container.operation.PhotoOperationInfo;

/**
 * Created by oleg on 18.02.2018.
 */

public class PhotoOperation extends Operation {

    private File photoFile;

    public PhotoOperation(Operation operation) {
        super(operation);
    }

    @Override
    public int getId() {
        return PHOTO;
    }

    public File getPhotoFile() {
        return photoFile;
    }

    public void setPhotoFile(File photoFile) {
        this.photoFile = photoFile;
    }

    @Override
    public boolean isCompleted() {
        return photoFile != null;
    }

    @Override
    public IFlexible createAdapterItem() {
        return new PhotoOperationInfo(this);
    }
}
