package ru.frosteye.urovotest.execution.operation;

import java.io.File;

import eu.davidea.flexibleadapter.items.IFlexible;
import ru.frosteye.urovotest.data.entity.container.operation.PhotoOperationInfo;
import ru.frosteye.urovotest.data.entity.container.operation.VIdeoOperationInfo;

/**
 * Created by oleg on 18.02.2018.
 */

public class VideoOperation extends Operation {

    private File videoFile;

    public VideoOperation(Operation operation) {
        super(operation);
    }

    @Override
    public int getId() {
        return VIDEO;
    }

    public File getVideoFile() {
        return videoFile;
    }

    public void setVideoFile(File videoFile) {
        this.videoFile = videoFile;
    }

    @Override
    public boolean isCompleted() {
        return videoFile != null;
    }

    @Override
    public IFlexible createAdapterItem() {
        return new VIdeoOperationInfo(this);
    }
}
