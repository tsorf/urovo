package ru.frosteye.urovotest.execution.task;

import java.util.concurrent.Executor;

import ru.frosteye.ovsa.execution.executor.MainThread;
import ru.frosteye.ovsa.execution.network.base.NetworkTask;
import ru.frosteye.urovotest.execution.exchange.common.Api;

/**
 * Created by oleg on 12.10.17.
 */

public abstract class BaseNetworkTask<P, R> extends NetworkTask<P, R> {

    private Api api;

    public BaseNetworkTask(MainThread mainThread,
                           Executor executor, Api api) {
        super(mainThread, executor);
        this.api = api;
    }

    public Api getApi() {
        return api;
    }
}
