package ru.frosteye.urovotest.execution.task;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Observable;
import ru.frosteye.ovsa.execution.executor.MainThread;
import ru.frosteye.ovsa.execution.network.response.MessageResponse;
import ru.frosteye.urovotest.app.environment.Keys;
import ru.frosteye.urovotest.data.repo.contract.CheckRepo;
import ru.frosteye.urovotest.data.repo.contract.TagsRepo;
import ru.frosteye.urovotest.execution.exchange.common.Api;

/**
 * Created by oleg on 12.10.17.
 */

public class CheckConnectionTask extends BaseNetworkTask<Void, Boolean> {

    private CheckRepo checkRepo;
    private TagsRepo tagsRepo;
    private Context context;
    private LocalBroadcastManager broadcastManager;

    @Inject
    public CheckConnectionTask(MainThread mainThread, Executor executor,
                               Api api, CheckRepo checkRepo,
                               TagsRepo tagsRepo, Context context) {
        super(mainThread, executor, api);
        this.checkRepo = checkRepo;
        this.tagsRepo = tagsRepo;
        this.context = context;
        this.broadcastManager = LocalBroadcastManager.getInstance(context);
    }

    @Override
    protected Observable<Boolean> prepareObservable(Void aVoid) {
        return Observable.interval(0, 5, TimeUnit.SECONDS).map(aLong -> {
            try {
                MessageResponse response = executeCall(getApi().ping());
                Boolean result = response.getCode() == 200;
                notifyByBroadcast(result);
                return result;
            } catch (Exception e) {
                return false;
            }
        });
    }

    private void notifyByBroadcast(boolean result) {
        Intent intent = new Intent(Keys.ACTION_UPDATE_CONNECTION_STATE);
        intent.putExtra(Keys.ACTION_UPDATE_CONNECTION_STATE, result);
        broadcastManager.sendBroadcast(intent);
    }
}
