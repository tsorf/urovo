package ru.frosteye.urovotest.execution.task;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;

import java.util.List;
import java.util.concurrent.Executor;

import javax.inject.Inject;

import io.reactivex.Observable;
import ru.frosteye.ovsa.execution.executor.MainThread;
import ru.frosteye.urovotest.app.environment.Keys;
import ru.frosteye.urovotest.data.entity.container.Checks;
import ru.frosteye.urovotest.data.entity.container.CodeInfo;
import ru.frosteye.urovotest.data.repo.contract.CheckRepo;
import ru.frosteye.urovotest.execution.exchange.common.Api;

/**
 * Created by oleg on 12.10.17.
 */

public class LoadCodesTask extends BaseNetworkTask<Void, List<CodeInfo>> {

    private CheckRepo checkRepo;
    private Context context;

    @Inject
    public LoadCodesTask(MainThread mainThread, Executor executor,
                         Api api, CheckRepo checkRepo, Context context) {
        super(mainThread, executor, api);
        this.checkRepo = checkRepo;
        this.context = context;
    }

    @Override
    protected Observable<List<CodeInfo>> prepareObservable(Void aVoid) {
        return Observable.create(subscriber -> {
            try {
                List<CodeInfo> codes = executeCall(getApi().list());
                checkRepo.executeAndSave(repo -> repo.fillRemoteItems(codes));
                subscriber.onNext(checkRepo.load().getAdapterItems());
                subscriber.onComplete();
            } catch (Exception e) {
                Checks checks = checkRepo.load();
                if(checks != null) {
                    subscriber.onNext(checks.getAdapterItems());
                } else {
                    subscriber.onError(e);
                }
            }
        });
    }
}
