package ru.frosteye.urovotest.execution.task;

import java.util.concurrent.Executor;

import javax.inject.Inject;

import io.reactivex.Observable;
import ru.frosteye.ovsa.execution.executor.MainThread;
import ru.frosteye.urovotest.data.entity.User;
import ru.frosteye.urovotest.data.repo.contract.TokenRepo;
import ru.frosteye.urovotest.data.repo.contract.UserRepo;
import ru.frosteye.urovotest.execution.exchange.common.Api;
import ru.frosteye.urovotest.execution.exchange.request.LoginRequest;
import ru.frosteye.urovotest.execution.exchange.response.LoginResponse;

/**
 * Created by oleg on 12.10.17.
 */

public class LoadProfileTask extends BaseNetworkTask<Void, User> {

    private UserRepo userRepo;

    @Inject
    public LoadProfileTask(MainThread mainThread, Executor executor,
                           Api api, UserRepo userRepo,
                           TokenRepo tokenRepo) {
        super(mainThread, executor, api);
        this.userRepo = userRepo;
    }

    @Override
    protected Observable<User> prepareObservable(Void aVoid) {
        return Observable.create(subscriber -> {
            try {
                User response = executeCall(getApi().profile());
                userRepo.save(response);
                subscriber.onNext(response);
                subscriber.onComplete();
                subscriber.onComplete();
            } catch (Exception e) {
                subscriber.onError(e);
            }
        });
    }
}
