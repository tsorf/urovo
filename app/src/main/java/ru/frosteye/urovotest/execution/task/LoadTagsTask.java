package ru.frosteye.urovotest.execution.task;

import java.util.List;
import java.util.concurrent.Executor;

import javax.inject.Inject;

import io.reactivex.Observable;
import ru.frosteye.ovsa.execution.executor.MainThread;
import ru.frosteye.urovotest.data.entity.Tag;
import ru.frosteye.urovotest.data.entity.Tags;
import ru.frosteye.urovotest.data.entity.container.CodeInfo;
import ru.frosteye.urovotest.data.repo.contract.TagsRepo;
import ru.frosteye.urovotest.execution.exchange.common.Api;

/**
 * Created by oleg on 12.10.17.
 */

public class LoadTagsTask extends BaseNetworkTask<Void, List<Tag>> {

    private TagsRepo tagsRepo;

    @Inject
    public LoadTagsTask(MainThread mainThread, Executor executor,
                        Api api, TagsRepo tagsRepo) {
        super(mainThread, executor, api);
        this.tagsRepo = tagsRepo;
    }

    @Override
    protected Observable<List<Tag>> prepareObservable(Void aVoid) {
        return Observable.create(subscriber -> {
            try {
                Tags tagsContainer = tagsRepo.load();
                if(tagsContainer != null && tagsContainer.getItems() != null) {
                    subscriber.onNext(tagsContainer.getItems());
                }
                List<Tag> tags = executeCall(getApi().listTags());
                tagsRepo.save(new Tags(tags));
                subscriber.onNext(tags);
                subscriber.onComplete();
            } catch (Exception e) {
                subscriber.onError(e);
            }
        });
    }
}
