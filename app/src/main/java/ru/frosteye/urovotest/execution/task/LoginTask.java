package ru.frosteye.urovotest.execution.task;

import com.crashlytics.android.Crashlytics;

import java.util.concurrent.Executor;

import javax.inject.Inject;

import io.reactivex.Observable;
import ru.frosteye.ovsa.execution.executor.MainThread;
import ru.frosteye.urovotest.data.entity.Tags;
import ru.frosteye.urovotest.data.repo.contract.TokenRepo;
import ru.frosteye.urovotest.data.repo.contract.UserRepo;
import ru.frosteye.urovotest.execution.exchange.common.Api;
import ru.frosteye.urovotest.execution.exchange.request.LoginRequest;
import ru.frosteye.urovotest.execution.exchange.response.LoginResponse;

/**
 * Created by oleg on 12.10.17.
 */

public class LoginTask extends BaseNetworkTask<LoginRequest, LoginResponse> {

    private UserRepo userRepo;
    private TokenRepo tokenRepo;

    @Inject
    public LoginTask(MainThread mainThread, Executor executor,
                     Api api, UserRepo userRepo,
                     TokenRepo tokenRepo) {
        super(mainThread, executor, api);
        this.userRepo = userRepo;
        this.tokenRepo = tokenRepo;
    }

    @Override
    protected Observable<LoginResponse> prepareObservable(LoginRequest loginRequest) {
        return Observable.create(subscriber -> {
            try {
                LoginResponse response = executeCall(getApi().auth(loginRequest));
                userRepo.save(response.getUser());
                tokenRepo.save(response.getToken());
                subscriber.onNext(response);
                subscriber.onComplete();
                subscriber.onComplete();
            } catch (Exception e) {
                Crashlytics.logException(e);
                subscriber.onError(e);
            }
        });
    }
}
