package ru.frosteye.urovotest.execution.task;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;

import java.util.Date;
import java.util.concurrent.Executor;

import javax.inject.Inject;

import io.reactivex.Observable;
import retrofit2.Call;
import ru.frosteye.ovsa.execution.executor.MainThread;
import ru.frosteye.urovotest.R;
import ru.frosteye.urovotest.app.environment.CodeParser;
import ru.frosteye.urovotest.app.environment.Keys;
import ru.frosteye.urovotest.data.entity.Code;
import ru.frosteye.urovotest.data.entity.CodeScan;
import ru.frosteye.urovotest.data.entity.TagContent;
import ru.frosteye.urovotest.data.entity.container.CodeInfo;
import ru.frosteye.urovotest.data.repo.contract.CheckRepo;
import ru.frosteye.urovotest.data.repo.contract.TagsRepo;
import ru.frosteye.urovotest.execution.exchange.common.Api;
import ru.frosteye.urovotest.execution.exchange.request.LogRequest;
import ru.frosteye.urovotest.execution.exchange.request.ReportRequest;
import ru.frosteye.urovotest.execution.exchange.request.ScanRequest;
import ru.frosteye.urovotest.execution.exchange.response.ResultResponse;
import ru.frosteye.urovotest.execution.operation.Operation;
import ru.frosteye.urovotest.execution.operation.PhotoOperation;
import ru.frosteye.urovotest.execution.operation.VideoOperation;

import static ru.frosteye.ovsa.data.storage.ResourceHelper.getString;

/**
 * Created by oleg on 12.10.17.
 */

public class ScanCodeTask extends BaseNetworkTask<ScanRequest, CodeScan> {

    private CheckRepo checkRepo;
    private TagsRepo tagsRepo;
    private CodeParser codeParser;
    private Context context;

    @Inject
    public ScanCodeTask(MainThread mainThread, Executor executor,
                        Api api, CheckRepo checkRepo,
                        TagsRepo tagsRepo,
                        CodeParser codeParser, Context context) {
        super(mainThread, executor, api);
        this.checkRepo = checkRepo;
        this.tagsRepo = tagsRepo;
        this.codeParser = codeParser;
        this.context = context;
    }

    @Override
    protected Observable<CodeScan> prepareObservable(ScanRequest request) {
        return Observable.create(subscriber -> {
            executeCall(getApi().log(new LogRequest("Отсканирован код " + request.getCodeValue() +
                    " в модуле " + request.getModule().getName())));
            Code code = new Code();
            code.setCodeType(request.getCodeType());
            code.setCodeValue(request.getCodeValue());
            code.setScannedAt(new Date());

            CodeScan codeScan = new CodeScan(code);

            for(Operation operation: request.getModule().getOperations()) {
                switch (operation.getId()) {
                    case Operation.PHOTO:
                        codeScan.getOperations().add(new PhotoOperation(operation));
                        break;
                    case Operation.VIDEO:
                        codeScan.getOperations().add(new VideoOperation(operation));
                        break;
                    default:
                        codeScan.getOperations().add(new Operation(operation));
                }
            }

            subscriber.onNext(codeScan);
            subscriber.onComplete();
            /*try {
                *//*CodeInfo code = executeCall(getApi().scan(request));
                TagContent tagContent = codeParser.extractTagContent(request.getCodeValue());
                if(tagContent != null) {
                    String taggedRequestResult = executeTaggedRequest(tagContent);
                    code.getModel().setRequestResult(taggedRequestResult);
                    executeCall(getApi().report(new ReportRequest(code.getModel().getScanId(), taggedRequestResult)));
                }*//*

            } catch (Exception e) {
                subscriber.onNext(codeInfo);
            }*/
        });
    }

    /*private String executeTaggedRequest(TagContent tagContent) {
        try {
            String url = tagContent.getTag().getFullUrl();
            if(url != null) {
                Call<String> call;
                switch (tagContent.getRequestType()) {
                    case POST:
                        call = getApi().tagPostRequest(url, tagContent.getParams());
                        break;
                    default:
                        call = getApi().tagGetRequest(url, tagContent.getParams());
                        break;
                }
                String result = executeCall(call);
                return getString(R.string.pattern_success_tagger, tagContent.getTag().getMethod(),
                        tagContent.getTag().getFullUrl(), result);
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return getString(R.string.pattern_error_tagger, tagContent.getTag().getMethod(),
                    tagContent.getTag().getFullUrl());
        }
    }*/

    /*private CodeInfo processLocal(ScanRequest request) {
        Code code = new Code()
                .setScannedAt(new Date())
                .setCodeValue(request.getCodeValue())
                .setCodeType(request.getCodeType())
                .setLocalCode(true);
        checkRepo.executeAndSave(repo -> {
            repo.getLocal().add(code);
            repo.createAdapterItems();
        });
        Intent intent = new Intent(Keys.ACTION_UPDATE_CONNECTION_STATE);
        intent.putExtra(Keys.ACTION_UPDATE_CONNECTION_STATE, checkRepo.load().getLocal().size());
        LocalBroadcastManager.getInstance(context)
                .sendBroadcast(intent);
        return new CodeInfo(code);
    }*/
}
