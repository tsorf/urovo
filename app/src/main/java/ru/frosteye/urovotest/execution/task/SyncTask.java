package ru.frosteye.urovotest.execution.task;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import java.util.List;
import java.util.concurrent.Executor;

import javax.inject.Inject;

import io.reactivex.Observable;
import ru.frosteye.ovsa.execution.executor.MainThread;
import ru.frosteye.urovotest.app.environment.Keys;
import ru.frosteye.urovotest.data.entity.Code;
import ru.frosteye.urovotest.data.entity.Tag;
import ru.frosteye.urovotest.data.entity.Tags;
import ru.frosteye.urovotest.data.entity.container.Checks;
import ru.frosteye.urovotest.data.entity.container.CodeInfo;
import ru.frosteye.urovotest.data.repo.contract.CheckRepo;
import ru.frosteye.urovotest.data.repo.contract.TagsRepo;
import ru.frosteye.urovotest.execution.exchange.common.Api;

/**
 * Created by oleg on 12.10.17.
 */

public class SyncTask extends BaseNetworkTask<Void, List<CodeInfo>> {

    private CheckRepo checkRepo;
    private TagsRepo tagsRepo;
    private Context context;

    @Inject
    public SyncTask(MainThread mainThread, Executor executor,
                    Api api, CheckRepo checkRepo,
                    TagsRepo tagsRepo, Context context) {
        super(mainThread, executor, api);
        this.checkRepo = checkRepo;
        this.tagsRepo = tagsRepo;
        this.context = context;
    }

    @Override
    protected Observable<List<CodeInfo>> prepareObservable(Void aVoid) {
        return Observable.create(subscriber -> {
            try {
                Checks checks = checkRepo.load();
                if(checks.getLocal() != null && !checks.getLocal().isEmpty()) {
                    List<Code> saved = executeCall(getApi().sync(checks.getLocal()));
                    for(Code code: saved) {
                        if(checks.getLocal().contains(code)) {
                            checks.getLocal().remove(code);
                        }
                    }
                    checkRepo.save(checks);
                    Intent intent = new Intent(Keys.ACTION_UPDATE_CONNECTION_STATE);
                    intent.putExtra(Keys.ACTION_UPDATE_CONNECTION_STATE, checkRepo.load().getLocal().size());
                    LocalBroadcastManager.getInstance(context)
                            .sendBroadcast(intent);
                }

                List<CodeInfo> codes = executeCall(getApi().list());
                checks.fillRemoteItems(codes);
                checkRepo.save(checks);

                List<Tag> tags = executeCall(getApi().listTags());
                tagsRepo.save(new Tags(tags));
                subscriber.onNext(checkRepo.load().getAdapterItems());
                subscriber.onComplete();
            } catch (Exception e) {
                subscriber.onError(e);
            }
        });
    }
}
