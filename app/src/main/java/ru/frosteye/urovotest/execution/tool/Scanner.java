package ru.frosteye.urovotest.execution.tool;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.device.ScanManager;
import android.device.scanner.configuration.PropertyID;

import com.crashlytics.android.Crashlytics;

import javax.inject.Inject;

import ru.frosteye.urovotest.app.environment.Keys;
import ru.frosteye.urovotest.data.entity.Code;
import ru.frosteye.urovotest.data.repo.contract.SettingsRepo;
import ru.frosteye.urovotest.media.Ringer;
import ru.frosteye.urovotest.presentation.view.impl.activity.CameraScanActivity;

/**
 * Created by oleg on 09.02.2018.
 */

public class Scanner {

    public interface Listener {
        void onNewCode(String code, String type);
    }

    private final static String SCAN_ACTION = ScanManager.ACTION_DECODE;

    private ScanManager scanManager;
    private Activity activity;
    private Ringer ringer;
    private String scanResult;
    private SettingsRepo settingsRepo;
    private Listener listener;
    private boolean isScanning;

    private BroadcastReceiver scanReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            setScanningStatus(false);

            byte[] barcode = intent.getByteArrayExtra(ScanManager.DECODE_DATA_TAG);
            int barcodelen = intent.getIntExtra(ScanManager.BARCODE_LENGTH_TAG, 0);
            byte temp = intent.getByteExtra(ScanManager.BARCODE_TYPE_TAG, (byte) 0);
            scanResult = new String(barcode, 0, barcodelen);
            activity.runOnUiThread(() -> {
                try {
                    if(settingsRepo.load().isBeep())
                        ringer.beep();
                } catch (Exception e) {
                    e.printStackTrace();
                    Crashlytics.logException(e);
                }
                if(settingsRepo.load().isScannerAutoTurnOff())
                    scanManager.stopDecode();
                String type = String.valueOf(temp);
                switch (temp) {
                    case 27:
                        type = Code.Type.DATA_MATRIX.toString();
                        break;
                    case 15:
                        type = Code.Type.SSCC.toString();
                        break;
                    case 11:
                        type = Code.Type.EAN13.toString();
                        break;
                    case 28:
                        type = Code.Type.QR_CODE.toString();
                        break;
                }
                onNewCode(scanResult, type);
            });
        }

    };

    @Inject
    public Scanner(Ringer ringer,
                   SettingsRepo settingsRepo) {
        this.ringer = ringer;
        this.settingsRepo = settingsRepo;
    }

    public void initWithActivity(Activity activity, Listener listener) {
        this.activity = activity;
        this.listener = listener;
    }

    public void scanWithCamera() {
        activity.startActivityForResult(new Intent(activity, CameraScanActivity.class), 99);
    }

    public void onResume() {

        try {
            scanManager = new ScanManager();
            scanManager.openScanner();
            scanManager.switchOutputMode(0);
            IntentFilter filter = new IntentFilter();
            int[] idbuf = new int[]{PropertyID.WEDGE_INTENT_ACTION_NAME, PropertyID.WEDGE_INTENT_DATA_STRING_TAG};
            String[] value_buf = scanManager.getParameterString(idbuf);
            if(value_buf != null && value_buf[0] != null && !value_buf[0].equals("")) {
                filter.addAction(value_buf[0]);
            } else {
                filter.addAction(SCAN_ACTION);
            }
            activity.registerReceiver(scanReceiver, filter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onNewCode(String code, String type) {
        listener.onNewCode(code, type);
    }

    public void onPause() {
        try {
            if(scanManager != null) {
                scanManager.stopDecode();
                setScanningStatus(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == Activity.RESULT_OK && requestCode == 99) {
            String code = data.getStringExtra(Keys.CODE);
            String type = data.getStringExtra(Keys.TYPE);
            onNewCode(code, type);
        }
    }

    private void setScanningStatus(boolean scanning) {
        this.isScanning = scanning;
    }
}
