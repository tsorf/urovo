package ru.frosteye.urovotest.media;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Vibrator;
import android.support.annotation.RawRes;


import javax.inject.Inject;

import ru.frosteye.urovotest.R;

/**
 * Created by ovcst on 17.05.2017.
 */

public class Ringer {

    private Context context;
    private MediaPlayer lastMediaPlayer;
    private static boolean enabled = true;
    private AudioManager audioManager;
    private Vibrator vibrator;

    @Inject
    public Ringer(Context context, AudioManager audioManager) {
        this.context = context;
        this.audioManager = audioManager;
        this.vibrator = ((Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE));
    }

    public static boolean isEnabled() {
        return enabled;
    }

    public static void setEnabled(boolean enabled) {
        Ringer.enabled = enabled;
    }

    public void beep() {
        if(!enabled) return;
        if(audioManager.getRingerMode() != AudioManager.RINGER_MODE_NORMAL) return;
        try {
            if(lastMediaPlayer != null && lastMediaPlayer.isPlaying()) return;
        } catch (Exception ignored) {

        }
        try {
            stop();
            lastMediaPlayer = MediaPlayer.create(context, R.raw.beep);
            lastMediaPlayer.setLooping(false);
            lastMediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void stop() {
        try {
            if(lastMediaPlayer == null) return;
            lastMediaPlayer.stop();
            lastMediaPlayer.release();
            lastMediaPlayer = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
