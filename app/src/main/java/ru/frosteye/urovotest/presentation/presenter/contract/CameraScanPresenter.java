package ru.frosteye.urovotest.presentation.presenter.contract;

import ru.frosteye.urovotest.presentation.view.contract.CameraScanView;

import ru.frosteye.ovsa.presentation.presenter.LivePresenter;

public interface CameraScanPresenter extends LivePresenter<CameraScanView> {
}
