package ru.frosteye.urovotest.presentation.presenter.contract;

import ru.frosteye.urovotest.presentation.view.contract.LoginView;

import ru.frosteye.ovsa.presentation.presenter.LivePresenter;

public interface LoginPresenter extends LivePresenter<LoginView> {
    void onLogin(String login, String password);
}
