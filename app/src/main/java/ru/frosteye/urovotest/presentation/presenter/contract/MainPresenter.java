package ru.frosteye.urovotest.presentation.presenter.contract;

import ru.frosteye.urovotest.presentation.view.contract.MainView;

import ru.frosteye.ovsa.presentation.presenter.LivePresenter;

public interface MainPresenter extends LivePresenter<MainView> {
    void onSync();
}
