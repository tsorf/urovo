package ru.frosteye.urovotest.presentation.presenter.contract;

import ru.frosteye.urovotest.presentation.view.contract.MenuView;

import ru.frosteye.ovsa.presentation.presenter.LivePresenter;

public interface MenuPresenter extends LivePresenter<MenuView> {
    void onLoadProfile();
    void onLoadMenu();
}
