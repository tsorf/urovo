package ru.frosteye.urovotest.presentation.presenter.contract;

import ru.frosteye.urovotest.data.entity.UiModule;
import ru.frosteye.urovotest.presentation.view.contract.ModuleView;

import ru.frosteye.ovsa.presentation.presenter.LivePresenter;

public interface ModulePresenter extends LivePresenter<ModuleView> {
    void onLoadModule(UiModule module);

    void onCodeScanned(String code, String type);
}
