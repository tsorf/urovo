package ru.frosteye.urovotest.presentation.presenter.contract;

import android.app.Activity;
import android.content.Intent;

import ru.frosteye.urovotest.presentation.view.contract.RfidView;

import ru.frosteye.ovsa.presentation.presenter.LivePresenter;

public interface RfidPresenter extends LivePresenter<RfidView> {
    void onNewIntent(Intent intent);
}
