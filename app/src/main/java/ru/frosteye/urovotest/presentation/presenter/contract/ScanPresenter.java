package ru.frosteye.urovotest.presentation.presenter.contract;

import ru.frosteye.ovsa.presentation.presenter.LivePresenter;
import ru.frosteye.urovotest.presentation.view.contract.ScanView;

/**
 * Created by oleg on 12.10.17.
 */

public interface ScanPresenter extends LivePresenter<ScanView> {
    void onLoadCodes();
    void onCodeScanned(String code, String type);
}
