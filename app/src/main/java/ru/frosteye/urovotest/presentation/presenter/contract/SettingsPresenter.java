package ru.frosteye.urovotest.presentation.presenter.contract;

import ru.frosteye.urovotest.data.entity.AppSettings;
import ru.frosteye.urovotest.presentation.view.contract.SettingsView;

import ru.frosteye.ovsa.presentation.presenter.LivePresenter;

public interface SettingsPresenter extends LivePresenter<SettingsView> {
    void onSettingsChanged(AppSettings settings);
    void onLogout();
}
