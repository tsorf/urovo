package ru.frosteye.urovotest.presentation.presenter.contract;

import ru.frosteye.urovotest.presentation.view.contract.TagsView;

import ru.frosteye.ovsa.presentation.presenter.LivePresenter;

public interface TagsPresenter extends LivePresenter<TagsView> {
    void onLoadTags();
}
