package ru.frosteye.urovotest.presentation.presenter.impl;

import javax.inject.Inject;

import ru.frosteye.urovotest.presentation.view.contract.CameraScanView;

import ru.frosteye.ovsa.presentation.presenter.BasePresenter;
import ru.frosteye.urovotest.presentation.presenter.contract.CameraScanPresenter;

public class CameraScanPresenterImpl extends BasePresenter<CameraScanView> implements CameraScanPresenter {

    @Inject
    public CameraScanPresenterImpl() {

    }

    @Override
    public void onDestroy() {

    }
}
