package ru.frosteye.urovotest.presentation.presenter.impl;

import javax.inject.Inject;

import ru.frosteye.ovsa.execution.task.SimpleSubscriber;
import ru.frosteye.urovotest.data.entity.User;
import ru.frosteye.urovotest.data.repo.contract.UserRepo;
import ru.frosteye.urovotest.execution.exchange.request.LoginRequest;
import ru.frosteye.urovotest.execution.exchange.response.LoginResponse;
import ru.frosteye.urovotest.execution.task.LoginTask;
import ru.frosteye.urovotest.presentation.view.contract.LoginView;

import ru.frosteye.ovsa.presentation.presenter.BasePresenter;
import ru.frosteye.urovotest.presentation.presenter.contract.LoginPresenter;

public class LoginPresenterImpl extends BasePresenter<LoginView> implements LoginPresenter {

    private UserRepo userRepo;
    private LoginTask loginTask;

    @Inject
    public LoginPresenterImpl(UserRepo userRepo, LoginTask loginTask) {

        this.userRepo = userRepo;
        this.loginTask = loginTask;
    }

    @Override
    public void onAttach(LoginView loginView) {
        super.onAttach(loginView);
        if(userRepo.load() != null) {
            view.proceed();
        }
    }

    @Override
    public void onDestroy() {
        loginTask.cancel();
    }

    @Override
    public void onLogin(String login, String password) {
        enableControls(false);
        loginTask.execute(new LoginRequest(login, password), new SimpleSubscriber<LoginResponse>() {
            @Override
            public void onError(Throwable e) {
                enableControls(true);
                showMessage(e.getMessage());
            }

            @Override
            public void onNext(LoginResponse user) {
                view.proceed();
            }
        });
    }
}
