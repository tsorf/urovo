package ru.frosteye.urovotest.presentation.presenter.impl;

import android.content.Context;

import java.util.List;

import javax.inject.Inject;

import ru.frosteye.ovsa.execution.task.SimpleSubscriber;
import ru.frosteye.urovotest.R;
import ru.frosteye.urovotest.data.entity.container.CodeInfo;
import ru.frosteye.urovotest.data.repo.contract.CheckRepo;
import ru.frosteye.urovotest.data.repo.contract.NFCRepo;
import ru.frosteye.urovotest.data.repo.contract.UserRepo;
import ru.frosteye.urovotest.execution.task.CheckConnectionTask;
import ru.frosteye.urovotest.execution.task.SyncTask;
import ru.frosteye.urovotest.presentation.view.contract.MainView;

import ru.frosteye.ovsa.presentation.presenter.BasePresenter;
import ru.frosteye.urovotest.presentation.presenter.contract.MainPresenter;

import static ru.frosteye.ovsa.data.storage.ResourceHelper.getString;

public class MainPresenterImpl extends BasePresenter<MainView> implements MainPresenter {

    private UserRepo userRepo;
    private CheckRepo checkRepo;
    private NFCRepo nfcRepo;
    private CheckConnectionTask checkConnectionTask;
    private SyncTask syncTask;
    private Context context;

    @Inject
    public MainPresenterImpl(UserRepo userRepo,
                             CheckRepo checkRepo,
                             NFCRepo nfcRepo,
                             CheckConnectionTask checkConnectionTask,
                             SyncTask syncTask, Context context) {

        this.userRepo = userRepo;
        this.checkRepo = checkRepo;
        this.nfcRepo = nfcRepo;
        this.checkConnectionTask = checkConnectionTask;
        this.syncTask = syncTask;
        this.context = context;
    }

    @Override
    public void onAttach(MainView mainView) {
        super.onAttach(mainView);
        view.showUser(userRepo.load());
        checkConnectionTask.execute(null, new SimpleSubscriber<Boolean>() {
            @Override
            public void onError(Throwable e) {
                view.setConnectionStatus(false);
            }

            @Override
            public void onNext(Boolean aBoolean) {
                view.setConnectionStatus(aBoolean);
            }
        });
    }

    @Override
    public void onDestroy() {
        syncTask.cancel();
        checkConnectionTask.cancel();
    }

    @Override
    public void onSync() {
        enableControls(false);
        syncTask.execute(null, new SimpleSubscriber<List<CodeInfo>>() {
            @Override
            public void onError(Throwable e) {
                enableControls(true);
                showError(e.getMessage());
            }

            @Override
            public void onNext(List<CodeInfo> infos) {
                enableControls(true);
                showSuccess(getString(R.string.synced));
            }
        });
    }


}
