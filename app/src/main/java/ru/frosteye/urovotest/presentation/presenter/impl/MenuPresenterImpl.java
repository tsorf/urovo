package ru.frosteye.urovotest.presentation.presenter.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import ru.frosteye.ovsa.execution.task.SimpleSubscriber;
import ru.frosteye.urovotest.data.entity.UiModule;
import ru.frosteye.urovotest.data.entity.User;
import ru.frosteye.urovotest.data.entity.container.UiModuleInfo;
import ru.frosteye.urovotest.data.repo.contract.UserRepo;
import ru.frosteye.urovotest.execution.task.LoadProfileTask;
import ru.frosteye.urovotest.presentation.view.contract.MenuView;

import ru.frosteye.ovsa.presentation.presenter.BasePresenter;
import ru.frosteye.urovotest.presentation.presenter.contract.MenuPresenter;

public class MenuPresenterImpl extends BasePresenter<MenuView> implements MenuPresenter {

    private UserRepo userRepo;
    private LoadProfileTask loadProfileTask;

    @Inject
    public MenuPresenterImpl(UserRepo userRepo,
                             LoadProfileTask loadProfileTask) {
        this.userRepo = userRepo;
        this.loadProfileTask = loadProfileTask;
    }

    @Override
    public void onAttach(MenuView menuView) {
        super.onAttach(menuView);
        onLoadMenu();
    }

    @Override
    public void onDestroy() {
        loadProfileTask.cancel();
    }

    @Override
    public void onLoadProfile() {
        enableControls(false);
        loadProfileTask.execute(null, new SimpleSubscriber<User>() {
            @Override
            public void onError(Throwable e) {
                enableControls(true);
                showError(e.getMessage());
            }

            @Override
            public void onNext(User user) {
                enableControls(true);
                onLoadMenu();
            }
        });
    }

    @Override
    public void onLoadMenu() {

        List<UiModuleInfo> infos = new ArrayList<>();
        for(UiModule module: userRepo.load().getRole().getModules()) {
            infos.add(new UiModuleInfo(module));
        }
        Collections.sort(infos);
        view.showMenu(infos);
    }
}
