package ru.frosteye.urovotest.presentation.presenter.impl;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;

import javax.inject.Inject;

import ru.frosteye.ovsa.execution.task.SimpleSubscriber;
import ru.frosteye.urovotest.app.environment.Keys;
import ru.frosteye.urovotest.data.entity.CodeScan;
import ru.frosteye.urovotest.data.entity.UiModule;
import ru.frosteye.urovotest.data.entity.container.CodeScanInfo;
import ru.frosteye.urovotest.execution.exchange.request.ScanRequest;
import ru.frosteye.urovotest.execution.task.ScanCodeTask;
import ru.frosteye.urovotest.presentation.view.contract.ModuleView;

import ru.frosteye.ovsa.presentation.presenter.BasePresenter;
import ru.frosteye.urovotest.presentation.presenter.contract.ModulePresenter;

public class ModulePresenterImpl extends BasePresenter<ModuleView> implements ModulePresenter {

    private ScanCodeTask scanCodeTask;
    private UiModule module;
    private Context context;

    @Inject
    public ModulePresenterImpl(ScanCodeTask scanCodeTask, Context context) {
        this.scanCodeTask = scanCodeTask;
        this.context = context;
    }

    @Override
    public void onAttach(ModuleView moduleView) {
        super.onAttach(moduleView);
        LocalBroadcastManager.getInstance(context)
            .registerReceiver(connectionReceiver, new IntentFilter(Keys.ACTION_UPDATE_CONNECTION_STATE));
    }

    @Override
    public void onDestroy() {
        scanCodeTask.cancel();
        LocalBroadcastManager.getInstance(context)
                .unregisterReceiver(connectionReceiver);
    }

    @Override
    public void onLoadModule(UiModule module) {
        this.module = module;
    }

    @Override
    public void onCodeScanned(String code, String type) {
        enableControls(false);
        scanCodeTask.execute(new ScanRequest(code, type, module), new SimpleSubscriber<CodeScan>() {
            @Override
            public void onError(Throwable e) {
                enableControls(true);
                showError(e.getMessage());
            }

            @Override
            public void onNext(CodeScan scan) {
                enableControls(true);
                view.appendCode(new CodeScanInfo(scan));
            }
        });
    }

    private BroadcastReceiver connectionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            view.setConnectionStatus(intent.getBooleanExtra(Keys.ACTION_UPDATE_CONNECTION_STATE, false));
        }
    };
}
