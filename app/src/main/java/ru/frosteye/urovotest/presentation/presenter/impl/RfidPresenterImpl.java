package ru.frosteye.urovotest.presentation.presenter.impl;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NfcAdapter;
import android.nfc.tech.IsoDep;
import android.nfc.tech.NfcF;
import android.nfc.tech.NfcV;

import javax.inject.Inject;

import ru.frosteye.ovsa.tool.TextTools;
import ru.frosteye.urovotest.presentation.view.contract.RfidView;

import ru.frosteye.ovsa.presentation.presenter.BasePresenter;
import ru.frosteye.urovotest.presentation.presenter.contract.RfidPresenter;
import ru.frosteye.urovotest.presentation.view.impl.activity.MainActivity;

public class RfidPresenterImpl extends BasePresenter<RfidView> implements RfidPresenter {

    public static final String[][] TECHLISTS = new String[][] {
            { IsoDep.class.getName() },
            { NfcV.class.getName() },
            { NfcF.class.getName() },
    };

    public static IntentFilter[] FILTERS;

    static {
        try {
            FILTERS = new IntentFilter[] { new IntentFilter(
                    NfcAdapter.ACTION_TECH_DISCOVERED, "*/*")};
        } catch (Exception e) {
        }
    }

    private NfcAdapter adapter;
    private Activity context;
    private PendingIntent pendingIntent;

    @Inject
    public RfidPresenterImpl() {
    }

    @Override
    public void onAttach(RfidView rfidView) {
        super.onAttach(rfidView);
        context = view.getActivity();
        adapter = NfcAdapter.getDefaultAdapter(context);
        pendingIntent = PendingIntent.getActivity(context, 0, new Intent(context,
                MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void onResume() {
        if (adapter != null)
            adapter.enableForegroundDispatch(context, pendingIntent, FILTERS, TECHLISTS);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (adapter != null)
            adapter.disableForegroundDispatch(context);
    }

    @Override
    public void onNewIntent(Intent intent) {

    }
}
