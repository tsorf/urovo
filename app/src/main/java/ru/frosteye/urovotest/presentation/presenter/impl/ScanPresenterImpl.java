package ru.frosteye.urovotest.presentation.presenter.impl;

import java.util.List;

import javax.inject.Inject;

import ru.frosteye.ovsa.execution.task.SimpleSubscriber;
import ru.frosteye.ovsa.presentation.presenter.BasePresenter;
import ru.frosteye.urovotest.data.entity.container.CodeInfo;
import ru.frosteye.urovotest.execution.exchange.request.ScanRequest;
import ru.frosteye.urovotest.execution.task.LoadCodesTask;
import ru.frosteye.urovotest.execution.task.ScanCodeTask;
import ru.frosteye.urovotest.execution.task.SyncTask;
import ru.frosteye.urovotest.presentation.presenter.contract.ScanPresenter;
import ru.frosteye.urovotest.presentation.view.contract.ScanView;

/**
 * Created by oleg on 12.10.17.
 */

public class ScanPresenterImpl extends BasePresenter<ScanView> implements ScanPresenter {

    private SyncTask syncTask;
    private LoadCodesTask loadCodesTask;
    private ScanCodeTask scanCodeTask;

    @Inject
    public ScanPresenterImpl(SyncTask syncTask,
                             LoadCodesTask loadCodesTask,
                             ScanCodeTask scanCodeTask) {
        this.syncTask = syncTask;
        this.loadCodesTask = loadCodesTask;
        this.scanCodeTask = scanCodeTask;
    }

    @Override
    public void onDestroy() {
        scanCodeTask.cancel();
        syncTask.cancel();
    }

    @Override
    public void onLoadCodes() {
        enableControls(false);
        loadCodesTask.execute(null, new SimpleSubscriber<List<CodeInfo>>() {
            @Override
            public void onError(Throwable e) {
                enableControls(true);
                showError(e.getMessage());
            }

            @Override
            public void onNext(List<CodeInfo> codeInfos) {
                enableControls(true);
                view.showCodes(codeInfos);
            }
        });
    }

    @Override
    public void onCodeScanned(String code, String type) {
        enableControls(false);
    }
}
