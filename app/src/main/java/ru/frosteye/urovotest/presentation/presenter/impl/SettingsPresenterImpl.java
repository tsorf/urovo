package ru.frosteye.urovotest.presentation.presenter.impl;

import javax.inject.Inject;

import ru.frosteye.urovotest.data.entity.AppSettings;
import ru.frosteye.urovotest.data.repo.contract.NFCRepo;
import ru.frosteye.urovotest.data.repo.contract.SettingsRepo;
import ru.frosteye.urovotest.data.repo.contract.UserRepo;
import ru.frosteye.urovotest.execution.exchange.common.ApiClient;
import ru.frosteye.urovotest.execution.task.SyncTask;
import ru.frosteye.urovotest.presentation.view.contract.SettingsView;

import ru.frosteye.ovsa.presentation.presenter.BasePresenter;
import ru.frosteye.urovotest.presentation.presenter.contract.SettingsPresenter;

import static ru.frosteye.ovsa.data.storage.ResourceHelper.getString;

public class SettingsPresenterImpl extends BasePresenter<SettingsView> implements SettingsPresenter {

    private SettingsRepo settingsRepo;
    private ApiClient apiClient;
    private NFCRepo nfcRepo;
    private UserRepo userRepo;
    private SyncTask syncTask;

    @Inject
    public SettingsPresenterImpl(SettingsRepo settingsRepo, ApiClient apiClient,
                                 NFCRepo nfcRepo,
                                 UserRepo userRepo,
                                 SyncTask syncTask) {

        this.settingsRepo = settingsRepo;
        this.apiClient = apiClient;
        this.nfcRepo = nfcRepo;
        this.userRepo = userRepo;
        this.syncTask = syncTask;
    }

    @Override
    public void onAttach(SettingsView settingsView) {
        super.onAttach(settingsView);
        view.showAppSettings(settingsRepo.load(), nfcRepo.load());
    }

    @Override
    public void onStop() {
        syncTask.cancel();
    }

    @Override
    public void onSettingsChanged(AppSettings settings) {
        settingsRepo.save(settings);
        apiClient.dropService();
    }

    @Override
    public void onLogout() {
        userRepo.save(null);
    }
}
