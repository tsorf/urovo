package ru.frosteye.urovotest.presentation.presenter.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import ru.frosteye.ovsa.execution.task.SimpleSubscriber;
import ru.frosteye.urovotest.data.entity.Tag;
import ru.frosteye.urovotest.data.entity.container.TagInfo;
import ru.frosteye.urovotest.execution.task.LoadTagsTask;
import ru.frosteye.urovotest.presentation.view.contract.TagsView;

import ru.frosteye.ovsa.presentation.presenter.BasePresenter;
import ru.frosteye.urovotest.presentation.presenter.contract.TagsPresenter;

public class TagsPresenterImpl extends BasePresenter<TagsView> implements TagsPresenter {

    private LoadTagsTask loadTagsTask;

    @Inject
    public TagsPresenterImpl(LoadTagsTask loadTagsTask) {

        this.loadTagsTask = loadTagsTask;
    }

    @Override
    public void onStop() {
        loadTagsTask.cancel();
    }

    @Override
    public void onLoadTags() {
        enableControls(false);
        loadTagsTask.execute(null, new SimpleSubscriber<List<Tag>>() {
            @Override
            public void onError(Throwable e) {
                enableControls(true);
                showError(e.getMessage());
            }

            @Override
            public void onComplete() {
                enableControls(true);
            }

            @Override
            public void onNext(List<Tag> tags) {
                List<TagInfo> tagInfos = new ArrayList<>();
                for(Tag tag: tags) {
                    tagInfos.add(new TagInfo(tag));
                }
                view.showTags(tagInfos);
            }
        });
    }
}
