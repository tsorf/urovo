package ru.frosteye.urovotest.presentation.view.contract;

import ru.frosteye.ovsa.presentation.view.BasicView;

public interface LoginView extends BasicView {
    void proceed();
}
