package ru.frosteye.urovotest.presentation.view.contract;

import android.app.Activity;

import ru.frosteye.ovsa.presentation.view.BasicView;
import ru.frosteye.urovotest.data.entity.User;

public interface MainView extends BasicView {
    void showUser(User user);
    void setConnectionStatus(boolean connected);
}
