package ru.frosteye.urovotest.presentation.view.contract;

import java.util.List;

import ru.frosteye.ovsa.presentation.view.BasicView;
import ru.frosteye.urovotest.data.entity.container.UiModuleInfo;

public interface MenuView extends BasicView {
    void showMenu(List<UiModuleInfo> items);
}
