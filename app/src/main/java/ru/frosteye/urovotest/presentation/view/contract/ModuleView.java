package ru.frosteye.urovotest.presentation.view.contract;

import ru.frosteye.ovsa.presentation.view.BasicView;
import ru.frosteye.urovotest.data.entity.container.CodeInfo;
import ru.frosteye.urovotest.data.entity.container.CodeScanInfo;

public interface ModuleView extends BasicView {
    void appendCode(CodeScanInfo codeInfo);
    void setConnectionStatus(boolean connected);
}
