package ru.frosteye.urovotest.presentation.view.contract;

import android.app.Activity;

import ru.frosteye.ovsa.presentation.view.BasicView;

public interface RfidView extends BasicView {
    Activity getActivity();
}
