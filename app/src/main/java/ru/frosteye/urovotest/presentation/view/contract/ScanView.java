package ru.frosteye.urovotest.presentation.view.contract;

import java.util.List;

import ru.frosteye.ovsa.presentation.view.BasicView;
import ru.frosteye.urovotest.data.entity.container.CodeInfo;

/**
 * Created by oleg on 12.10.17.
 */

public interface ScanView extends BasicView {
    void showCodes(List<CodeInfo> list);
    void appendCode(CodeInfo codeInfo);
}
