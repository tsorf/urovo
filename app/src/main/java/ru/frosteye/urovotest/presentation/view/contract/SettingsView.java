package ru.frosteye.urovotest.presentation.view.contract;

import ru.frosteye.ovsa.presentation.view.BasicView;
import ru.frosteye.urovotest.data.entity.AppSettings;
import ru.frosteye.urovotest.data.entity.NFCEntity;

public interface SettingsView extends BasicView {
    void showAppSettings(AppSettings settings, NFCEntity nfcEntity);
}
