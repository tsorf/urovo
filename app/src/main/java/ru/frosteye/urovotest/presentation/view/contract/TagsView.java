package ru.frosteye.urovotest.presentation.view.contract;

import java.util.List;

import ru.frosteye.ovsa.presentation.view.BasicView;
import ru.frosteye.urovotest.data.entity.container.TagInfo;

public interface TagsView extends BasicView {
    void showTags(List<TagInfo> tagInfos);
}
