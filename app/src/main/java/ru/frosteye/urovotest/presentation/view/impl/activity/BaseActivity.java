package ru.frosteye.urovotest.presentation.view.impl.activity;

import android.content.Context;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;

import javax.inject.Inject;

import ru.frosteye.urovotest.app.di.component.PresenterComponent;
import ru.frosteye.urovotest.app.di.module.PresenterModule;
import ru.frosteye.urovotest.app.environment.LogoutManager;
import ru.frosteye.urovotest.app.environment.Urovo;

import butterknife.ButterKnife;
import ru.frosteye.ovsa.presentation.view.activity.PresenterActivity;

/**
 * Created by ovcst on 01.05.2017.
 */

public abstract class BaseActivity extends PresenterActivity {

    @Inject LogoutManager logoutManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PresenterComponent component = Urovo.getAppComponent().plus(new PresenterModule(this));
        component.inject(this);
        inject(component);
    }

    @Override
    protected void onResume() {
        super.onResume();
        logoutManager.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        logoutManager.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        logoutManager.onDestroy();
    }

    protected abstract void inject(PresenterComponent component);

    @Override
    protected void prepareView() {
        ButterKnife.bind(this);
    }
}
