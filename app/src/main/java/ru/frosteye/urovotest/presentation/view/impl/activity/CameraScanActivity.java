package ru.frosteye.urovotest.presentation.view.impl.activity;

import javax.inject.Inject;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.google.zxing.Result;

import me.dm7.barcodescanner.zxing.ZXingScannerView;
import ru.frosteye.urovotest.app.di.component.PresenterComponent;
import ru.frosteye.urovotest.app.environment.Keys;
import ru.frosteye.urovotest.presentation.presenter.contract.CameraScanPresenter;
import ru.frosteye.urovotest.presentation.view.contract.CameraScanView;
import ru.frosteye.ovsa.presentation.presenter.LivePresenter;
import ru.frosteye.ovsa.presentation.view.activity.PresenterActivity;
import ru.frosteye.urovotest.R;

public class CameraScanActivity extends BaseActivity implements CameraScanView, ZXingScannerView.ResultHandler {

    private ZXingScannerView scannerView;

    @Inject CameraScanPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        scannerView = new ZXingScannerView(this);   // Programmatically initialize the scanner view
        setContentView(scannerView);
    }

    @Override
    public void enableControls(boolean enabled, int code) {

    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        super.initView(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        scannerView.setResultHandler(this);
        scannerView.startCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        scannerView.stopCamera();
    }

    @Override
    protected void attachPresenter() {
        presenter.onAttach(this);
    }

    @Override
    protected LivePresenter<?> getPresenter() {
        return presenter;
    }

    @Override
    protected void inject(PresenterComponent component) {
        component.inject(this);
    }

    @Override
    public void handleResult(Result result) {
        Intent intent = new Intent();
        intent.putExtra(Keys.CODE, result.getText());
        intent.putExtra(Keys.TYPE, result.getBarcodeFormat().toString());
        setResult(RESULT_OK, intent);
        finish();
    }
}
