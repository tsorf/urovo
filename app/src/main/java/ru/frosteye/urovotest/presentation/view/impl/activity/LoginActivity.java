package ru.frosteye.urovotest.presentation.view.impl.activity;

import javax.inject.Inject;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import butterknife.BindView;
import ru.frosteye.ovsa.tool.TextTools;
import ru.frosteye.urovotest.app.di.component.PresenterComponent;
import ru.frosteye.urovotest.presentation.presenter.contract.LoginPresenter;
import ru.frosteye.urovotest.presentation.view.contract.LoginView;
import ru.frosteye.ovsa.presentation.presenter.LivePresenter;
import ru.frosteye.urovotest.R;
import ru.frosteye.urovotest.presentation.view.impl.fragment.ScanFragment;

public class LoginActivity extends BaseActivity implements LoginView {

    @BindView(R.id.activity_login_enter) Button enter;
    @BindView(R.id.activity_login_login) EditText login;
    @BindView(R.id.activity_login_password) EditText password;
    @BindView(R.id.activity_login_progress) ProgressBar progress;

    @Inject LoginPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    @Override
    public void enableControls(boolean enabled, int code) {
        enter.setEnabled(enabled);
        login.setEnabled(enabled);
        password.setEnabled(enabled);
        progress.setVisibility(enabled ? View.INVISIBLE : View.VISIBLE);
    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        registerTextChangeListeners(s -> {
            enter.setEnabled(!TextTools.isTrimmedEmpty(login)
            && !TextTools.isTrimmedEmpty(password));
        }, login, password);
        enter.setOnClickListener(v -> {
            String loginString = TextTools.extractTrimmed(login);
            String passwordString = TextTools.extractTrimmed(password);
            presenter.onLogin(loginString, passwordString);
        });
    }

    @Override
    protected void attachPresenter() {
        presenter.onAttach(this);
    }

    @Override
    protected LivePresenter<?> getPresenter() {
        return presenter;
    }

    @Override
    protected void inject(PresenterComponent component) {
        component.inject(this);
    }

    @Override
    public void proceed() {
        startActivity(MainActivity.class);
        finish();
    }
}
