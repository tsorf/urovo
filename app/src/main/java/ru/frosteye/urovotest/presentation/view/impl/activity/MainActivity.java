package ru.frosteye.urovotest.presentation.view.impl.activity;

import javax.inject.Inject;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.nfc.Tag;
import android.nfc.tech.MifareClassic;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;

import butterknife.BindView;
import ru.frosteye.ovsa.tool.NetworkTools;
import ru.frosteye.ovsa.tool.TextTools;
import ru.frosteye.urovotest.app.di.component.PresenterComponent;
import ru.frosteye.urovotest.data.entity.User;
import ru.frosteye.urovotest.data.repo.contract.SettingsRepo;
import ru.frosteye.urovotest.presentation.presenter.contract.MainPresenter;
import ru.frosteye.urovotest.presentation.view.contract.MainView;
import ru.frosteye.ovsa.presentation.presenter.LivePresenter;
import ru.frosteye.urovotest.R;
import ru.frosteye.urovotest.presentation.view.impl.fragment.BaseFragment;
import ru.frosteye.urovotest.presentation.view.impl.fragment.MenuFragment;
import ru.frosteye.urovotest.presentation.view.impl.fragment.SettingsFragment;

public class MainActivity extends BaseActivity implements MainView {

    @BindView(R.id.activity_main_container) FrameLayout container;
    @BindView(R.id.activity_main_nav) AHBottomNavigation nav;

    @Inject MainPresenter presenter;
    @Inject SettingsRepo settingsRepo;

    private BaseFragment currentFragment;
    private MenuItem connectionIcon;
    private Boolean lastConnected;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(settingsRepo.load().shouldSleep()) {
            getWindow().addFlags(View.KEEP_SCREEN_ON);
        }
        setContentView(R.layout.activity_main);
        onNewIntent(getIntent());
    }

    @Override
    public void enableControls(boolean enabled, int code) {
        super.enableControls(enabled, code);
        showTopBarLoading(!enabled);
    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        nav.addItem(new AHBottomNavigationItem(getString(R.string.modules), R.drawable.ic_grid));
        nav.addItem(new AHBottomNavigationItem(getString(R.string.settings), R.drawable.ic_settings));
        nav.setOnTabSelectedListener((position, wasSelected) -> {
            showFragment(position);
            return true;
        });
        nav.setAccentColor(getResources().getColor(R.color.colorPrimary));
        showFragment(0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        connectionIcon = menu.findItem(R.id.action_status);
        if(lastConnected != null) {
            connectionIcon.setIcon(lastConnected ? R.drawable.connect : R.drawable.disconnect);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void showUser(User user) {
        setTitle(user.getFormattedName());
    }

    @Override
    public void setConnectionStatus(boolean connected) {
        try {
            lastConnected = connected;
            connectionIcon.setIcon(connected ? R.drawable.connect : R.drawable.disconnect);
        } catch (Exception ignored) {}
    }

    private void showFragment(int position) {
        switch (position) {
            case 0:
                showFragment(new MenuFragment());
                break;
            case 1:
                showFragment(new SettingsFragment());
                break;
        }
    }

    private void showFragment(BaseFragment fragment) {
        currentFragment = fragment;
        getSupportFragmentManager()
                .beginTransaction()
                .replace(container.getId(), fragment)
                .commit();
    }

    @Override
    protected void attachPresenter() {
        presenter.onAttach(this);
    }

    @Override
    protected LivePresenter<?> getPresenter() {
        return presenter;
    }

    @Override
    protected void inject(PresenterComponent component) {
        component.inject(this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

    }

    private void processTag(Tag tag) {
        String[] techList = tag.getTechList();
        StringBuilder techString = new StringBuilder();
        for (int i = 0; i < techList.length; i++) {
            techString.append(techList[i]);
            techString.append(";\n");
        }
        // 获取卡id
        byte[] id = tag.getId();
        String content = TextTools.byteArrayToHexString(id);

        MifareClassic mifareClassic = MifareClassic.get(tag);
        if (mifareClassic != null) {

        } else {

        }
    }
}
