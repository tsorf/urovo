package ru.frosteye.urovotest.presentation.view.impl.activity;

import javax.inject.Inject;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.levibostian.shutter_android.Shutter;
import com.levibostian.shutter_android.builder.ShutterResultCallback;
import com.levibostian.shutter_android.builder.ShutterResultListener;
import com.levibostian.shutter_android.vo.ShutterResult;

import java.util.ArrayList;

import butterknife.BindView;
import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.flexibleadapter.items.IFlexible;
import ru.frosteye.ovsa.presentation.adapter.FlexibleModelAdapter;
import ru.frosteye.ovsa.presentation.view.InteractiveModelView;
import ru.frosteye.ovsa.presentation.view.widget.ListDivider;
import ru.frosteye.urovotest.app.di.component.PresenterComponent;
import ru.frosteye.urovotest.app.environment.Actions;
import ru.frosteye.urovotest.app.environment.Keys;
import ru.frosteye.urovotest.data.entity.CodeScan;
import ru.frosteye.urovotest.data.entity.UiModule;
import ru.frosteye.urovotest.data.entity.container.CodeScanInfo;
import ru.frosteye.urovotest.execution.operation.Operation;
import ru.frosteye.urovotest.execution.operation.PhotoOperation;
import ru.frosteye.urovotest.execution.operation.VideoOperation;
import ru.frosteye.urovotest.execution.tool.Scanner;
import ru.frosteye.urovotest.presentation.presenter.contract.ModulePresenter;
import ru.frosteye.urovotest.presentation.view.contract.ModuleView;
import ru.frosteye.ovsa.presentation.presenter.LivePresenter;
import ru.frosteye.urovotest.R;

public class ModuleActivity extends BaseActivity implements ModuleView, Scanner.Listener,
        FlexibleAdapter.OnItemClickListener, FlexibleAdapter.OnItemLongClickListener, InteractiveModelView.Listener {

    @BindView(R.id.activity_module_codes) RecyclerView codes;
    @BindView(R.id.activity_module_scan) FloatingActionButton scan;
    @BindView(R.id.activity_module_tools) RecyclerView tools;
    @BindView(R.id.activity_module_empty) View empty;

    @Inject ModulePresenter presenter;
    @Inject Scanner scanner;

    private UiModule module;
    private CodeScan codeScan;
    private MenuItem connectionIcon;
    private boolean lastConnected = true;

    private ShutterResultListener shutterResultListener;
    private FlexibleModelAdapter<CodeScanInfo> codesAdapter;
    private FlexibleModelAdapter<IFlexible> toolsAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_module);
    }

    @Override
    public void enableControls(boolean enabled, int code) {
        super.enableControls(enabled, code);
        showTopBarLoading(!enabled);
    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        enableBackButton();
        module = getSerializable(Keys.MODULE, UiModule.class);
        setTitle(module.getName());
        scan.setOnClickListener(v -> scanner.scanWithCamera());

        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setStackFromEnd(true);
        codes.setLayoutManager(manager);
        codesAdapter = new FlexibleModelAdapter<>(new ArrayList<>(), this, null);
        codes.addItemDecoration(new ListDivider(this, ListDivider.VERTICAL_LIST));
        codes.setAdapter(codesAdapter);

        toolsAdapter = new FlexibleModelAdapter<>(this);
        tools.setLayoutManager(new GridLayoutManager(this, 2));
        tools.setAdapter(toolsAdapter);
    }

    @Override
    protected void attachPresenter() {
        scanner.initWithActivity(this, this);
        presenter.onAttach(this);
        presenter.onLoadModule(module);
    }

    @Override
    protected void onPause() {
        super.onPause();
        scanner.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        scanner.onResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        scanner.onActivityResult(requestCode, resultCode, data);
        if(shutterResultListener != null)
            shutterResultListener.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected LivePresenter<?> getPresenter() {
        return presenter;
    }

    @Override
    protected void inject(PresenterComponent component) {
        component.inject(this);
    }

    @Override
    public void onNewCode(String code, String type) {
        presenter.onCodeScanned(code, type);
    }

    @Override
    public void appendCode(CodeScanInfo codeInfo) {
        empty.setVisibility(View.GONE);
        codesAdapter.addItem(codeInfo);
        codes.smoothScrollToPosition(codesAdapter.getItemCount());
        setCurrentCode(codeInfo.getModel());
    }

    @Override
    public boolean onItemClick(int position) {
        CodeScan scan = codesAdapter.getItem(position).getModel();
        setCurrentCode(scan);
        return true;
    }

    @Override
    public void onItemLongClick(int position) {
        codesAdapter.removeItem(position);
    }

    private void setCurrentCode(CodeScan scan) {
        for(int i = 0; i < codesAdapter.getItemCount(); i++) {
            codesAdapter.getItem(i).getModel().setSelected(false);
        }
        scan.setSelected(true);
        codeScan = scan;
        codesAdapter.notifyDataSetChanged();
        toolsAdapter.clear();
        toolsAdapter.addItems(0, scan.createAdapterItems());
    }

    @Override
    public void onModelAction(int code, Object payload) {
        switch (code) {
            case Actions.TAKE_PHOTO:
                takePhoto(((PhotoOperation) payload));
                break;
            case Actions.TAKE_VIDEO:
                takeVideo(((VideoOperation) payload));
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        connectionIcon = menu.findItem(R.id.action_status);
        connectionIcon.setIcon(lastConnected ? R.drawable.connect : R.drawable.disconnect);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void setConnectionStatus(boolean connected) {
        try {
            lastConnected = connected;
            connectionIcon.setIcon(connected ? R.drawable.connect : R.drawable.disconnect);
        } catch (Exception ignored) {}
    }

    private void takePhoto(PhotoOperation operation) {
        shutterResultListener = Shutter.with(this)
                .takePhoto()
                .usePrivateAppExternalStorage()
                .snap(new ShutterResultCallback() {
                    @Override
                    public void onComplete(ShutterResult shutterResult) {
                        operation.setPhotoFile(shutterResult.mediaFile());
                        checkOperations();
                    }

                    @Override
                    public void onError(String s, Throwable throwable) {
                        showError(s);
                    }
                });
    }

    private void takeVideo(VideoOperation operation) {
        shutterResultListener = Shutter.with(this)
                .recordVideo()
                .usePrivateAppExternalStorage()
                .snap(new ShutterResultCallback() {
                    @Override
                    public void onComplete(ShutterResult shutterResult) {
                        operation.setVideoFile(shutterResult.mediaFile());
                        checkOperations();
                    }

                    @Override
                    public void onError(String s, Throwable throwable) {
                        showError(s);
                    }
                });
    }

    private void checkOperations() {
        toolsAdapter.notifyDataSetChanged();
        for(Operation operation: codeScan.getOperations()) {
            if(!operation.isCompleted()) return;
        }
        codeScan.setCompleted(true);
        codesAdapter.notifyDataSetChanged();
    }
}
