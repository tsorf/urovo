package ru.frosteye.urovotest.presentation.view.impl.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import ru.frosteye.ovsa.data.storage.ResourceHelper;
import ru.frosteye.ovsa.presentation.presenter.LivePresenter;
import ru.frosteye.urovotest.R;
import ru.frosteye.urovotest.app.di.component.PresenterComponent;

/**
 * Created by oleg on 16.11.2017.
 */

public class AdminFragment extends BaseFragment {

    @BindView(R.id.fragment_admin_pager) ViewPager pager;
    @BindView(R.id.fragment_admin_tabs) TabLayout tabs;

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_admin;
    }

    @Override
    protected void initView(View view, Bundle savedInstanceState) {
        pager.setAdapter(new Adapter(getChildFragmentManager()));
        tabs.setupWithViewPager(pager);
    }

    @Override
    protected void attachPresenter() {

    }

    @Override
    protected LivePresenter<?> getPresenter() {
        return null;
    }

    @Override
    public void onNewCode(String code, String type) {

    }

    @Override
    protected void inject(PresenterComponent component) {

    }

    @Override
    public CharSequence getTitle() {
        return ResourceHelper.getString(R.string.admin);
    }

    private class Adapter extends FragmentPagerAdapter {

        private List<BaseFragment> fragments = new ArrayList<>();

        public Adapter(FragmentManager fm) {
            super(fm);
//            fragments.add(new TagsFragment());
            fragments.add(new SettingsFragment());
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragments.get(position).getTitle();
        }

        @Override
        public int getCount() {
            return fragments.size();
        }
    }
}
