package ru.frosteye.urovotest.presentation.view.impl.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import ru.frosteye.urovotest.app.di.component.PresenterComponent;
import ru.frosteye.urovotest.app.di.module.PresenterModule;
import ru.frosteye.urovotest.app.environment.Urovo;

import butterknife.ButterKnife;
import ru.frosteye.ovsa.presentation.view.fragment.PresenterFragment;

public abstract class BaseFragment extends PresenterFragment {

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        PresenterComponent component = Urovo.getAppComponent().plus(new PresenterModule(this));
        component.inject(this);
        inject(component);
    }

    @Override
    protected void prepareView(View view) {
        ButterKnife.bind(this, view);
    }

    public String createTag() {
        return getClass().getSimpleName();
    }

    public void onNewCode(String code, String type) {

    }

    protected abstract void inject(PresenterComponent component);

    public abstract CharSequence getTitle();

}
