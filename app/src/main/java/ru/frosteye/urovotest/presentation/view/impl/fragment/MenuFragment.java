package ru.frosteye.urovotest.presentation.view.impl.fragment;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import javax.inject.Inject;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import eu.davidea.flexibleadapter.FlexibleAdapter;
import ru.frosteye.ovsa.data.storage.ResourceHelper;
import ru.frosteye.ovsa.presentation.adapter.FlexibleModelAdapter;
import ru.frosteye.urovotest.app.di.component.PresenterComponent;
import ru.frosteye.urovotest.app.environment.Keys;
import ru.frosteye.urovotest.data.entity.UiModule;
import ru.frosteye.urovotest.data.entity.container.UiModuleInfo;
import ru.frosteye.urovotest.presentation.presenter.contract.MenuPresenter;
import ru.frosteye.urovotest.presentation.view.contract.MenuView;
import ru.frosteye.ovsa.presentation.presenter.LivePresenter;
import ru.frosteye.urovotest.R;
import ru.frosteye.urovotest.presentation.view.impl.activity.ModuleActivity;

public class MenuFragment extends BaseFragment implements MenuView, FlexibleAdapter.OnItemClickListener {

    @BindView(R.id.fragment_menu_grid) RecyclerView grid;
    @BindView(R.id.fragment_menu_swipe) SwipeRefreshLayout swipe;

    @Inject MenuPresenter presenter;

    private FlexibleModelAdapter<UiModuleInfo> adapter;

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_menu;
    }

    @Override
    public void enableControls(boolean enabled, int code) {
        showTopBarLoading(!enabled);
        if(enabled) swipe.setRefreshing(false);
    }

    @Override
    protected void initView(View view, Bundle savedInstanceState) {
        adapter = new FlexibleModelAdapter<>(new ArrayList<>(), this);
        grid.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        grid.setAdapter(adapter);
        swipe.setOnRefreshListener(() -> presenter.onLoadProfile());
    }

    @Override
    protected void attachPresenter() {
        presenter.onAttach(this);
    }

    @Override
    protected LivePresenter<?> getPresenter() {
        return presenter;
    }

    @Override
    protected void inject(PresenterComponent component) {
        component.inject(this);
    }

    @Override
    public CharSequence getTitle() {
        return ResourceHelper.getString(R.string.modules);
    }

    @Override
    public void showMenu(List<UiModuleInfo> items) {
        adapter.updateDataSet(items);
    }

    @Override
    public boolean onItemClick(int position) {
        UiModule module = adapter.getItem(position).getModel();
        Intent intent = new Intent(getActivity(), ModuleActivity.class);
        intent.putExtra(Keys.MODULE, module);
        startActivity(intent);
        return true;
    }
}
