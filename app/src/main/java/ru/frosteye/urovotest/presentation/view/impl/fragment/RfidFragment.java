package ru.frosteye.urovotest.presentation.view.impl.fragment;

import android.content.Intent;
import android.view.View;

import javax.inject.Inject;

import android.os.Bundle;

import ru.frosteye.ovsa.data.storage.ResourceHelper;
import ru.frosteye.urovotest.app.di.component.PresenterComponent;
import ru.frosteye.urovotest.presentation.presenter.contract.RfidPresenter;
import ru.frosteye.urovotest.presentation.view.contract.RfidView;
import ru.frosteye.ovsa.presentation.presenter.LivePresenter;
import ru.frosteye.urovotest.R;

public class RfidFragment extends BaseFragment implements RfidView {

    @Inject RfidPresenter presenter;

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_rfid;
    }

    @Override
    public void enableControls(boolean enabled, int code) {

    }

    @Override
    protected void initView(View view, Bundle savedInstanceState) {

    }

    @Override
    protected void attachPresenter() {
        presenter.onAttach(this);
    }

    @Override
    protected LivePresenter<?> getPresenter() {
        return presenter;
    }

    @Override
    public void onNewCode(String code, String type) {

    }

    @Override
    protected void inject(PresenterComponent component) {
        component.inject(this);
    }

    @Override
    public CharSequence getTitle() {
        return ResourceHelper.getString(R.string.rfid);
    }
}
