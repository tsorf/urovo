package ru.frosteye.urovotest.presentation.view.impl.fragment;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.device.ScanManager;
import android.device.scanner.configuration.PropertyID;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.crashlytics.android.Crashlytics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import ru.frosteye.ovsa.data.storage.ResourceHelper;
import ru.frosteye.ovsa.presentation.adapter.FlexibleModelAdapter;
import ru.frosteye.ovsa.presentation.presenter.LivePresenter;
import ru.frosteye.ovsa.presentation.view.widget.ListDivider;
import ru.frosteye.urovotest.R;
import ru.frosteye.urovotest.app.di.component.PresenterComponent;
import ru.frosteye.urovotest.app.environment.Keys;
import ru.frosteye.urovotest.data.entity.Code;
import ru.frosteye.urovotest.data.entity.container.CodeInfo;
import ru.frosteye.urovotest.data.repo.contract.SettingsRepo;
import ru.frosteye.urovotest.media.Ringer;
import ru.frosteye.urovotest.presentation.presenter.contract.ScanPresenter;
import ru.frosteye.urovotest.presentation.view.contract.ScanView;
import ru.frosteye.urovotest.presentation.view.impl.activity.CameraScanActivity;

public class ScanFragment extends BaseFragment implements ScanView {

    private final static String SCAN_ACTION = ScanManager.ACTION_DECODE;

    @BindView(R.id.fragment_scan_swipe) SwipeRefreshLayout swipe;
    @BindView(R.id.fragment_scan_list) RecyclerView list;
    @BindView(R.id.fragment_scan_startStop) FloatingActionButton startStop;

    @Inject ScanPresenter presenter;
    @Inject SettingsRepo settingsRepo;
    @Inject Ringer ringer;

    private String scanResult;
    private ScanManager scanManager;
    private FlexibleModelAdapter<CodeInfo> adapter;

    private boolean isScanning;

    private BroadcastReceiver scanReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            setScanningStatus(false);

            byte[] barcode = intent.getByteArrayExtra(ScanManager.DECODE_DATA_TAG);
            int barcodelen = intent.getIntExtra(ScanManager.BARCODE_LENGTH_TAG, 0);
            byte temp = intent.getByteExtra(ScanManager.BARCODE_TYPE_TAG, (byte) 0);
            scanResult = new String(barcode, 0, barcodelen);
            getActivity().runOnUiThread(() -> {
                try {
                    if(settingsRepo.load().isBeep())
                        ringer.beep();
                } catch (Exception e) {
                    e.printStackTrace();
                    Crashlytics.logException(e);
                }
                if(settingsRepo.load().isScannerAutoTurnOff())
                    scanManager.stopDecode();
                String type = String.valueOf(temp);
                switch (temp) {
                    case 27:
                        type = Code.Type.DATA_MATRIX.toString();
                        break;
                    case 15:
                        type = Code.Type.SSCC.toString();
                        break;
                    case 11:
                        type = Code.Type.EAN13.toString();
                        break;
                    case 28:
                        type = Code.Type.QR_CODE.toString();
                        break;
                }
                onNewCode(scanResult, type);
            });
        }

    };

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_scan;
    }

    @Override
    protected void prepareView(View view) {
        super.prepareView(view);
        startStop.setOnClickListener(v -> {
            startActivityForResult(new Intent(getActivity(), CameraScanActivity.class), 99);
            /*if(isScanning) {
                stopScan();
            } else {
                startScan();
            }*/
        });
        /*continuous.setOnClickListener(v -> {
            if(scanManager.getTriggerMode() != Triggering.CONTINUOUS)
                scanManager.setTriggerMode(Triggering.CONTINUOUS);
        });*/
        adapter = new FlexibleModelAdapter<>(new ArrayList<>());
        list.setLayoutManager(new LinearLayoutManager(getActivity()));
        list.addItemDecoration(new ListDivider(getActivity(), ListDivider.VERTICAL_LIST));
        list.setAdapter(adapter);
        swipe.setOnRefreshListener(() -> presenter.onLoadCodes());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Activity.RESULT_OK && requestCode == 99) {
            String code = data.getStringExtra(Keys.CODE);
            String type = data.getStringExtra(Keys.TYPE);
            onNewCode(code, type);
        }
    }

    private void setScanningStatus(boolean scanning) {
        this.isScanning = scanning;
        this.startStop.setBackgroundColor(scanning ?
            getResources().getColor(R.color.colorRed) : getResources().getColor(R.color.colorGreen));
    }

    @Override
    public void onNewCode(String code, String type) {
        presenter.onCodeScanned(code, type);
    }

    private void startScan() {
        try {
            scanManager.stopDecode();
        } catch (Exception e) {
            e.printStackTrace();
        }
        setScanningStatus(true);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        scanManager.startDecode();
    }

    private void stopScan() {
        scanManager.stopDecode();
    }

    @Override
    protected void attachPresenter() {
        presenter.onAttach(this);
        LocalBroadcastManager.getInstance(getActivity())
                .registerReceiver(counterReceiver, new IntentFilter(Keys.ACTION_UPDATE_CONNECTION_STATE));
        presenter.onLoadCodes();
    }

    @Override
    protected LivePresenter<?> getPresenter() {
        return presenter;
    }

    @Override
    protected void inject(PresenterComponent component) {
        component.inject(this);
    }

    @Override
    public CharSequence getTitle() {
        return ResourceHelper.getString(R.string.scan);
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            if(scanManager != null) {
                scanManager.stopDecode();
                setScanningStatus(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            getActivity().unregisterReceiver(scanReceiver);
        } catch (Exception e) {}
    }

    private BroadcastReceiver counterReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            getActivity().runOnUiThread(() -> presenter.onLoadCodes());
        }
    };

    @Override
    public void onResume() {
        super.onResume();

        try {
            scanManager = new ScanManager();
            scanManager.openScanner();
            scanManager.switchOutputMode(0);
            IntentFilter filter = new IntentFilter();
            int[] idbuf = new int[]{PropertyID.WEDGE_INTENT_ACTION_NAME, PropertyID.WEDGE_INTENT_DATA_STRING_TAG};
            String[] value_buf = scanManager.getParameterString(idbuf);
            if(value_buf != null && value_buf[0] != null && !value_buf[0].equals("")) {
                filter.addAction(value_buf[0]);
            } else {
                filter.addAction(SCAN_ACTION);
            }
            getActivity().registerReceiver(scanReceiver, filter);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void enableControls(boolean enabled, int code) {
        swipe.setRefreshing(!enabled);
    }

    @Override
    public void showCodes(List<CodeInfo> list) {
        Collections.reverse(list);
        adapter.updateDataSet(list, true);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(counterReceiver);
        } catch (Exception e) { }
    }

    @Override
    public void appendCode(CodeInfo codeInfo) {
        adapter.addItem(0, codeInfo);
        try {
            list.smoothScrollToPosition(0);
        } catch (Exception e) {}
    }
}
