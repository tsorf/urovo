package ru.frosteye.urovotest.presentation.view.impl.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.SwitchCompat;
import android.text.Editable;
import android.view.View;

import javax.inject.Inject;

import android.os.Bundle;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import butterknife.BindView;
import ru.frosteye.ovsa.data.storage.ResourceHelper;
import ru.frosteye.ovsa.presentation.callback.SimpleTextChangeCallback;
import ru.frosteye.ovsa.presentation.view.dialog.Confirm;
import ru.frosteye.ovsa.tool.TextTools;
import ru.frosteye.urovotest.app.di.component.PresenterComponent;
import ru.frosteye.urovotest.data.entity.AppSettings;
import ru.frosteye.urovotest.data.entity.NFCEntity;
import ru.frosteye.urovotest.presentation.presenter.contract.SettingsPresenter;
import ru.frosteye.urovotest.presentation.view.contract.SettingsView;
import ru.frosteye.ovsa.presentation.presenter.LivePresenter;
import ru.frosteye.urovotest.R;
import ru.frosteye.urovotest.presentation.view.impl.activity.LoginActivity;

public class SettingsFragment extends BaseFragment implements SettingsView {

    @BindView(R.id.fragment_settings_switch) SwitchCompat sleep;
    @BindView(R.id.fragment_settings_url) EditText url;
    @BindView(R.id.fragment_settings_rfarm) EditText rfarm;
    @BindView(R.id.fragment_settings_rfoperation) EditText rfoperation;
    @BindView(R.id.fragment_settings_rfserial) EditText rfserial;
    @BindView(R.id.fragment_settings_rfuser) EditText rfuser;
    @BindView(R.id.fragment_settings_save) Button save;
    @BindView(R.id.fragment_settings_defaults) Button defaults;
    @BindView(R.id.fragment_settings_scanner) SwitchCompat scanner;
    @BindView(R.id.fragment_settings_showTags) SwitchCompat tags;
    @BindView(R.id.fragment_settings_beep) SwitchCompat beep;
    @BindView(R.id.fragment_settings_progress) ProgressBar progress;
    @BindView(R.id.fragment_settings_logout) View logout;

    @Inject SettingsPresenter presenter;

    private AppSettings appSettings;
    private NFCEntity nfcEntity;

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_settings;
    }

    @Override
    public void enableControls(boolean enabled, int code) {
        logout.setEnabled(enabled);
        save.setEnabled(enabled);
        defaults.setEnabled(enabled);
        scanner.setEnabled(enabled);
        url.setEnabled(enabled);
        tags.setEnabled(enabled);
        beep.setEnabled(enabled);
        rfarm.setEnabled(enabled);
        rfoperation.setEnabled(enabled);
        rfserial.setEnabled(enabled);
        rfuser.setEnabled(enabled);
        progress.setVisibility(enabled ? View.INVISIBLE : View.VISIBLE);
    }

    @Override
    protected void initView(View view, Bundle savedInstanceState) {
        save.setOnClickListener(v -> {
            String urlString = TextTools.extractTrimmed(url);
            if(!URLUtil.isValidUrl(urlString)) {
                showMessage(getString(R.string.invalid_url));
                return;
            }
            if(!urlString.endsWith("/")) {
                showMessage(getString(R.string.invalid_url_ending));
                return;
            }
            appSettings.setServerUrl(urlString);
            appSettings.setSleep(sleep.isChecked());
            appSettings.setShowFoundTags(tags.isChecked());
            appSettings.setScannerAutoTurnOff(scanner.isChecked());
            appSettings.setBeep(beep.isChecked());
            presenter.onSettingsChanged(appSettings);
            showMessage(getString(R.string.saved));
        });
        defaults.setOnClickListener(v -> {
            appSettings.setSleep(false);
            appSettings.setScannerAutoTurnOff(true);
            appSettings.setBeep(true);
            appSettings.setShowFoundTags(true);
            appSettings.setServerUrl(getString(R.string.config_api_url));
            presenter.onSettingsChanged(appSettings);
            showAppSettings(appSettings, nfcEntity);
            showMessage(getString(R.string.saved));
        });
        logout.setOnClickListener(v -> {
            Confirm.create(getActivity())
                    .title(R.string.logout)
                    .message(R.string.sure)
                    .no(R.string.cancel, (dialogInterface, i) -> dialogInterface.cancel())
                    .yes(R.string.logout, (dialogInterface, i) -> {
                        presenter.onLogout();
                        startActivity(new Intent(getActivity(), LoginActivity.class));
                        getActivity().finish();
                    }).show();
        });
    }

    @Override
    protected void attachPresenter() {
        presenter.onAttach(this);
    }

    @Override
    protected LivePresenter<?> getPresenter() {
        return presenter;
    }

    @Override
    public void onNewCode(String code, String type) {

    }

    @Override
    protected void inject(PresenterComponent component) {
        component.inject(this);
    }

    @Override
    public CharSequence getTitle() {
        return ResourceHelper.getString(R.string.settings);
    }

    @Override
    public void showAppSettings(AppSettings settings, NFCEntity entity) {
        this.appSettings = settings;
        this.nfcEntity = entity;
        url.setText(settings.getServerUrl());
        sleep.setChecked(settings.shouldSleep());
        scanner.setChecked(settings.isScannerAutoTurnOff());
        beep.setChecked(settings.isBeep());
        tags.setChecked(settings.isShowFoundTags());
        rfarm.setText(entity.getRfarm());
        rfoperation.setText(entity.getRfoperation());
        rfserial.setText(entity.getRfserial());
        rfuser.setText(entity.getRfuser());
    }
}
