package ru.frosteye.urovotest.presentation.view.impl.fragment;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import javax.inject.Inject;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import ru.frosteye.ovsa.data.storage.ResourceHelper;
import ru.frosteye.ovsa.presentation.adapter.FlexibleModelAdapter;
import ru.frosteye.ovsa.presentation.view.widget.ListDivider;
import ru.frosteye.urovotest.app.di.component.PresenterComponent;
import ru.frosteye.urovotest.data.entity.container.TagInfo;
import ru.frosteye.urovotest.presentation.presenter.contract.TagsPresenter;
import ru.frosteye.urovotest.presentation.view.contract.TagsView;
import ru.frosteye.ovsa.presentation.presenter.LivePresenter;
import ru.frosteye.urovotest.R;

public class TagsFragment extends BaseFragment implements TagsView {

    @BindView(R.id.fragment_tags_list) RecyclerView list;
    @BindView(R.id.fragment_tags_swipe) SwipeRefreshLayout swipe;

    @Inject TagsPresenter presenter;

    private FlexibleModelAdapter<TagInfo> adapter;

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_tags;
    }

    @Override
    public void enableControls(boolean enabled, int code) {
        swipe.setRefreshing(!enabled);
    }

    @Override
    protected void initView(View view, Bundle savedInstanceState) {
        list.setLayoutManager(new LinearLayoutManager(getActivity()));
        list.addItemDecoration(new ListDivider(getActivity(), ListDivider.VERTICAL_LIST));
        adapter = new FlexibleModelAdapter<>(new ArrayList<>());
        list.setAdapter(adapter);
        swipe.setOnRefreshListener(() -> presenter.onLoadTags());
    }

    @Override
    protected void attachPresenter() {
        presenter.onAttach(this);
        presenter.onLoadTags();
    }

    @Override
    protected LivePresenter<?> getPresenter() {
        return presenter;
    }

    @Override
    public void onNewCode(String code, String type) {

    }

    @Override
    protected void inject(PresenterComponent component) {
        component.inject(this);
    }

    @Override
    public CharSequence getTitle() {
        return ResourceHelper.getString(R.string.tags);
    }

    @Override
    public void showTags(List<TagInfo> tagInfos) {
        adapter.updateDataSet(tagInfos);
    }
}
