package ru.frosteye.urovotest.presentation.view.impl.widget;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.text.Html;
import android.text.format.DateUtils;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.frosteye.ovsa.presentation.view.ModelView;
import ru.frosteye.ovsa.presentation.view.widget.BaseLinearLayout;
import ru.frosteye.ovsa.tool.DateTools;
import ru.frosteye.urovotest.R;
import ru.frosteye.urovotest.app.environment.CodeParser;
import ru.frosteye.urovotest.app.environment.Urovo;
import ru.frosteye.urovotest.data.entity.Code;
import ru.frosteye.urovotest.data.entity.CodeScan;
import ru.frosteye.urovotest.data.entity.ParsedCode;

/**
 * Created by oleg on 18.02.2018.
 */

public class CodeRowView extends BaseLinearLayout implements ModelView<CodeScan> {


    @BindView(R.id.view_codeRow_date) TextView date;
    @BindView(R.id.view_codeRow_value) TextView value;
    @BindView(R.id.view_codeRow_type) TextView type;
    @BindView(R.id.view_codeRow_icon) ImageView icon;
    @BindView(R.id.view_codeRow_status) ImageView status;

    private CodeScan model;
    private CodeParser codeParser;

    public CodeRowView(Context context) {
        super(context);
    }

    public CodeRowView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CodeRowView(Context context, AttributeSet attrs,
                       int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public CodeRowView(Context context, AttributeSet attrs,
                       int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void prepareView() {
        if(isInEditMode()) return;
        ButterKnife.bind(this);
        codeParser = Urovo.getAppComponent().codeParser();
    }

    @Override
    public CodeScan getModel() {
        return model;
    }

    @Override
    public void setModel(CodeScan model) {
        this.model = model;
        ParsedCode parsedCode = codeParser.createParsedCode(model.getCode());
        this.type.setText(String.format("%s %s", model.getCode().getType().toString(), parsedCode.getCodeTypePostfix()));
        this.date.setText(DateUtils.getRelativeTimeSpanString(getContext(), model.getCode().getScannedAt().getTime()));
        this.icon.setImageResource(model.getCode().getType().getIcon());
        this.value.setText(Html.fromHtml(parsedCode.format()));
        this.status.setImageResource(model.isCompleted() ? R.drawable.ic_done : R.drawable.ic_await);
        if(model.isSelected()) {
            setBackgroundColor(getResources().getColor(R.color.colorBrandSemi));
        } else {
            setBackgroundResource(R.drawable.ovsa_bg_selector_simple);
        }
    }
}
