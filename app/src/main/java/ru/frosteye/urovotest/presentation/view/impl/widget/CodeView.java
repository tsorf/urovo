package ru.frosteye.urovotest.presentation.view.impl.widget;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.text.Html;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.frosteye.ovsa.presentation.view.ModelView;
import ru.frosteye.ovsa.presentation.view.widget.BaseLinearLayout;
import ru.frosteye.ovsa.tool.DateTools;
import ru.frosteye.urovotest.R;
import ru.frosteye.urovotest.app.environment.CodeParser;
import ru.frosteye.urovotest.app.environment.Urovo;
import ru.frosteye.urovotest.data.entity.Code;
import ru.frosteye.urovotest.data.entity.ParsedCode;

/**
 * Created by oleg on 12.10.17.
 */

public class CodeView extends BaseLinearLayout implements ModelView<Code> {

    @BindView(R.id.view_code_data) TextView data;
    @BindView(R.id.view_code_date) TextView date;
    @BindView(R.id.view_code_value) TextView value;
    @BindView(R.id.view_code_type) TextView type;
    @BindView(R.id.view_code_icon) ImageView icon;
    @BindView(R.id.view_code_tagger) TextView tagger;

    private CodeParser codeParser;

    private Code model;

    public CodeView(Context context) {
        super(context);
    }

    public CodeView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CodeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public CodeView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void prepareView() {
        ButterKnife.bind(this);
        codeParser = Urovo.getAppComponent().codeParser();
    }

    public Code getModel() {
        return model;
    }

    public void setModel(Code model) {
        this.model = model;
        ParsedCode parsedCode = codeParser.createParsedCode(model);
        this.type.setText(String.format("%s %s", model.getType().toString(), parsedCode.getCodeTypePostfix()));
        this.date.setText(DateTools.formatDottedDateWithTime(model.getScannedAt()));
        this.icon.setImageResource(model.getType().getIcon());
        this.value.setText(Html.fromHtml(parsedCode.format()));

        if(model.isLocalCode()) {
            this.data.setText(Html.fromHtml(getContext().getString(R.string.not_synced)));
            setBackgroundColor(getContext().getResources().getColor(R.color.colorLocal));
        } else {
            this.data.setText(model.getCodeContents());
            setBackgroundColor(Color.WHITE);
        }
        if(model.getRequestResult() == null || model.getRequestResult().isEmpty()) {
            tagger.setVisibility(GONE);
        } else {
            tagger.setVisibility(VISIBLE);
            tagger.setText(model.getRequestResult());
        }
    }
}
