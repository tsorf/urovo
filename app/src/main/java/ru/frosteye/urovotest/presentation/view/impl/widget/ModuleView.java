package ru.frosteye.urovotest.presentation.view.impl.widget;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.frosteye.ovsa.presentation.view.ModelView;
import ru.frosteye.ovsa.presentation.view.widget.BaseLinearLayout;
import ru.frosteye.ovsa.presentation.view.widget.BaseRelativeLayout;
import ru.frosteye.ovsa.tool.UITools;
import ru.frosteye.urovotest.R;
import ru.frosteye.urovotest.data.entity.Tag;
import ru.frosteye.urovotest.data.entity.UiModule;

/**
 * Created by oleg on 12.10.17.
 */

public class ModuleView extends BaseRelativeLayout implements ModelView<UiModule> {

    @BindView(R.id.view_module_title) TextView title;
    @BindView(R.id.view_module_icon) ImageView icon;

    private UiModule model;

    public ModuleView(Context context) {
        super(context);
    }

    public ModuleView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ModuleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public ModuleView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }

    @Override
    protected void prepareView() {
        ButterKnife.bind(this);
    }

    public UiModule getModel() {
        return model;
    }

    public void setModel(UiModule model) {
        this.model = model;
        this.title.setText(model.getName());
        Picasso.with(getContext()).load(model.getIcon()).fit().centerInside().into(icon);
    }
}
