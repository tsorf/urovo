package ru.frosteye.urovotest.presentation.view.impl.widget;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;

import butterknife.ButterKnife;
import ru.frosteye.ovsa.presentation.view.InteractiveModelView;
import ru.frosteye.ovsa.presentation.view.ModelView;
import ru.frosteye.ovsa.presentation.view.widget.BaseFrameLayout;
import ru.frosteye.urovotest.execution.operation.Operation;

/**
 * Created by oleg on 18.02.2018.
 */

public class OperationView<T extends Operation> extends BaseFrameLayout implements InteractiveModelView<T> {

    private T model;
    protected Listener listener;

    public OperationView(Context context) {
        super(context);
    }

    public OperationView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public OperationView(Context context, AttributeSet attrs,
                         int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public OperationView(Context context, AttributeSet attrs,
                         int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void prepareView() {
        if(isInEditMode()) return;
        ButterKnife.bind(this);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }

    @Override
    public T getModel() {
        return model;
    }

    @Override
    public void setModel(T model) {
        this.model = model;
    }

    @Override
    public void setListener(Listener listener) {
        this.listener = listener;
    }
}
