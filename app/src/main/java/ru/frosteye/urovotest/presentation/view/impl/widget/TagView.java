package ru.frosteye.urovotest.presentation.view.impl.widget;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.text.Html;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.frosteye.ovsa.presentation.view.ModelView;
import ru.frosteye.ovsa.presentation.view.widget.BaseLinearLayout;
import ru.frosteye.ovsa.tool.DateTools;
import ru.frosteye.urovotest.R;
import ru.frosteye.urovotest.app.environment.CodeParser;
import ru.frosteye.urovotest.app.environment.Urovo;
import ru.frosteye.urovotest.data.entity.Code;
import ru.frosteye.urovotest.data.entity.ParsedCode;
import ru.frosteye.urovotest.data.entity.Tag;

/**
 * Created by oleg on 12.10.17.
 */

public class TagView extends BaseLinearLayout implements ModelView<Tag> {

    @BindView(R.id.view_tag_id) TextView id;
    @BindView(R.id.view_tag_tag) TextView tag;
    @BindView(R.id.view_tag_url) TextView url;
    @BindView(R.id.view_tag_params) TextView params;
    @BindView(R.id.view_tag_method) TextView method;
    @BindView(R.id.view_tag_value) TextView value;

    private Tag model;

    public TagView(Context context) {
        super(context);
    }

    public TagView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TagView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public TagView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void prepareView() {
        ButterKnife.bind(this);
    }

    public Tag getModel() {
        return model;
    }

    public void setModel(Tag model) {
        this.model = model;
        this.url.setText(model.getUrl());
        this.params.setText(model.getParams());
        this.method.setText(model.getMethod());
        this.value.setText(model.getValue());
        this.id.setText(String.valueOf(model.getId()));
        this.tag.setText(model.getTag());
    }
}
