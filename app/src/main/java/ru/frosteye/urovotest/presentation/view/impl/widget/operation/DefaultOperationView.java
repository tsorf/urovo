package ru.frosteye.urovotest.presentation.view.impl.widget.operation;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import ru.frosteye.urovotest.R;
import ru.frosteye.urovotest.app.environment.Actions;
import ru.frosteye.urovotest.execution.operation.Operation;
import ru.frosteye.urovotest.execution.operation.VideoOperation;
import ru.frosteye.urovotest.presentation.view.impl.widget.OperationView;

/**
 * Created by oleg on 18.02.2018.
 */

public class DefaultOperationView extends OperationView<Operation> {

    @BindView(R.id.view_operation_default_title) TextView title;

    public DefaultOperationView(Context context) {
        super(context);
    }

    public DefaultOperationView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DefaultOperationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public DefaultOperationView(Context context, AttributeSet attrs, int defStyleAttr,
                                int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void prepareView() {
        super.prepareView();
    }

    @Override
    public void setModel(Operation model) {
        super.setModel(model);
        title.setText(model.getName());
    }
}
