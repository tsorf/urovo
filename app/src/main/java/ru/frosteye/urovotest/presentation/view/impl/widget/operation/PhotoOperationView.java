package ru.frosteye.urovotest.presentation.view.impl.widget.operation;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import ru.frosteye.urovotest.R;
import ru.frosteye.urovotest.app.environment.Actions;
import ru.frosteye.urovotest.execution.operation.PhotoOperation;
import ru.frosteye.urovotest.presentation.view.impl.widget.OperationView;

/**
 * Created by oleg on 18.02.2018.
 */

public class PhotoOperationView extends OperationView<PhotoOperation> {

    @BindView(R.id.view_operation_photo_image) ImageView photo;

    public PhotoOperationView(Context context) {
        super(context);
    }

    public PhotoOperationView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PhotoOperationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public PhotoOperationView(Context context, AttributeSet attrs, int defStyleAttr,
                              int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void prepareView() {
        super.prepareView();
        setOnClickListener(v -> listener.onModelAction(Actions.TAKE_PHOTO, getModel()));
    }

    @Override
    public void setModel(PhotoOperation model) {
        super.setModel(model);
        if(model.getPhotoFile() != null) {
            Picasso.with(getContext()).load(model.getPhotoFile()).fit().centerCrop().into(photo);
        } else {
            photo.setImageDrawable(null);
        }
    }
}
