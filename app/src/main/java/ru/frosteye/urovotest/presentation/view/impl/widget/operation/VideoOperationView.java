package ru.frosteye.urovotest.presentation.view.impl.widget.operation;

import android.content.Context;
import android.graphics.Color;
import android.media.ThumbnailUtils;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import ru.frosteye.urovotest.R;
import ru.frosteye.urovotest.app.environment.Actions;
import ru.frosteye.urovotest.execution.operation.PhotoOperation;
import ru.frosteye.urovotest.execution.operation.VideoOperation;
import ru.frosteye.urovotest.presentation.view.impl.widget.OperationView;

/**
 * Created by oleg on 18.02.2018.
 */

public class VideoOperationView extends OperationView<VideoOperation> {

    @BindView(R.id.view_operation_video_image) ImageView photo;

    public VideoOperationView(Context context) {
        super(context);
    }

    public VideoOperationView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public VideoOperationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public VideoOperationView(Context context, AttributeSet attrs, int defStyleAttr,
                              int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void prepareView() {
        super.prepareView();
        setOnClickListener(v -> listener.onModelAction(Actions.TAKE_VIDEO, getModel()));
    }

    @Override
    public void setModel(VideoOperation model) {
        super.setModel(model);
        if(model.getVideoFile() != null) {
            photo.setImageBitmap(ThumbnailUtils
                    .createVideoThumbnail(model.getVideoFile().getPath(),
                            MediaStore.Images.Thumbnails.MINI_KIND));
        } else {
            photo.setImageDrawable(null);
        }
    }
}
